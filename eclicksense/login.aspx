﻿<%@ Page Language="C#" MasterPageFile="~/mst_main.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" Title="Login Page" %>
  	
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js'></script>
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js'></script>
	<script src='js/slidetounlock.js'></script>

    </asp:content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphbody">
   <h2> <asp:Label ID="lblwrong" runat="server" Text=""></asp:Label></h2>
<br /><br />

<div id="well">
			
			<h2><strong id="slider"></strong> <span>slide to See Login</span></h2>
			
		</div>
<div id="pnl">
<table cellspacing="10px" cellpadding="5px" >
    <tr><td>Username</td>
    <td>
        <asp:TextBox ID="txtusername" CssClass="tb2" runat="server"></asp:TextBox></td></tr>
    <tr><td>Password</td>
    <td>
        <asp:TextBox ID="txtpassword" TextMode="Password" CssClass="tb2" runat="server"></asp:TextBox></td></tr>
        <tr><td></td></tr>
        <tr><td></td><td  align="right">
            <asp:Button ID="Button1" CssClass="button" runat="server" Text="Login" OnClick="login_Click"  /></td></tr>
</table>
</div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="right">
<a href="forget_username_pass.aspx">Forget Username & Password</a>
</asp:Content>