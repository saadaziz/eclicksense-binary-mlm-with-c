﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    db_control ob_userinfo = new db_control();
    cs_vars vars = new cs_vars();
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hpllogin");
        hpl.CssClass = "active";
        HttpCookie cookie = Request.Cookies["UserInfo"];
        if (cookie != null)
        /*if cookie exists, user is logged in, wants to logout, so kill cookie */
        {
            //objckusername.Expires = DateTime.Now.AddDays(-1);
            //Response.Cookies.Add(objckusername);

            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            Session.RemoveAll();
            Session.Abandon();
        }


    }

    public static bool isEmail(string inputEmail)
    {
        //inputEmail = NulltoString(inputEmail);
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return (true);
        else
            return (false);
    }
    protected void login_Click(object sender, EventArgs e)
    {
        string uname = txtusername.Text;
        string pass = txtpassword.Text;
        if (uname != "" && pass != "")
            if (uname.Contains("admin") || uname.Contains("fran"))
            {
                if (ob_userinfo.checkLoginadmin(uname, pass))
                {

                    Random _r = new Random();
                    HttpCookie cookie = new HttpCookie("UserInfo");
                    cookie["UserName"] = uname;
                    cookie.Expires = DateTime.Now.AddMinutes(20);

                    // query check1 = new query();
                    // cookie["UserId"] = check1.GetUserID(uname, pass);
                    //cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);

                    Session["UserName"] = uname;

                    Int64 rand = _r.Next(9999999);
                    vars.set_admin(rand);
                    int min_user = ob_userinfo.get_min_userid();
                    Session.Add("eclicksense", Convert.ToString(min_user));
                    Session.Add("root", Convert.ToString(min_user));

                    Session.Add("admin_ram", Convert.ToString(1));
                    txtpassword.Text = "";
                    txtusername.Text = "";
                    Response.Redirect("admin/admin2113.aspx");

                }
                else
                {
                    Int32 cnt_str = Convert.ToInt32(Session["count"]);
                    cnt_str++;
                    Session["count"] = Convert.ToString(cnt_str);
                    lblwrong.Text = "Wrong username and password";
                }
            }
            else 
            {
                if (ob_userinfo.checkLogin(uname, pass))
                {

                    Int32 userid = ob_userinfo.get_userid_uname(uname);
//vars.set_userid(userid);


                    //string scheme = ob_userinfo.get_scheme(userid);

                    HttpCookie cookie = new HttpCookie("UserInfo");
                    cookie["UserName"] = uname;
                    cookie.Expires = DateTime.Now.AddHours(1);

                    // query check1 = new query();
                    // cookie["UserId"] = check1.GetUserID(uname, pass);
                    //cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);

                    Session["UserName"] = uname;
                    Session.Add("eclicksense", Convert.ToString(userid));
                    Session.Add("root", Convert.ToString(userid));
                    ob_userinfo.insert_login(vars.get_userid(), GetIP());
                    txtpassword.Text = "";
                    txtusername.Text = "";
                    Response.Redirect("users/profile.aspx");
                }
                else
                {
                    Int32 cnt_str = Convert.ToInt32(Session["count"]);
                    cnt_str++;
                    Session["count"] = Convert.ToString(cnt_str);
                    lblwrong.Text = "Wrong username or password for users";
                    //ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script language='javascript'>alert('Wrong Username and Password');</script>");

                }
            }
            else
            {
                Int32 cnt_str = Convert.ToInt32(Session["count"]);
                cnt_str++;
                Session["count"] = Convert.ToString(cnt_str);
                lblwrong.Text = "Wrong username & Password";
                //ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script language='javascript'>alert('Wrong Username and Password');</script>");

            }

    }
    protected string GetIP()
    {
        string strHostName = "";
        strHostName = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strHostName == null)

            strHostName = Request.ServerVariables["REMOTE_ADDR"];

        return strHostName;
        //return Dns.GetHostName();

    }
}