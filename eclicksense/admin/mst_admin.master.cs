﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mst_admin : System.Web.UI.MasterPage
{
    int log = 0;
    cs_vars vars = new cs_vars();
    protected void Page_Load(object sender, EventArgs e)
    {
        string username_c = (string)null;
        string username_s = (string)Session["UserName"];
        HttpCookie cookie = Request.Cookies["UserInfo"];

        if (cookie != null)
        {
            username_c = cookie["UserName"];
            if ((username_c != null) && (username_s == username_c) && (vars.get_userid() != 0))
            {
                log = 1;
            }

        }
        if (log == 0 && Convert.ToInt32(HttpContext.Current.Session["admin_ram"]) == 0)
            Response.Redirect("../login.aspx");
    }
}
