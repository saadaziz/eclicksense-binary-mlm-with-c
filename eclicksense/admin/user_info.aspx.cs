﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class user_info : System.Web.UI.Page
{
    cs_vars vars = new cs_vars();
    db_admin ob_userinfo = new db_admin();
    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 tot_user = (ob_userinfo.get_total_user()/50)+1;
        for (int i = 1; i <= tot_user; i++)
            ddlpage.Items.Add(i.ToString());
        string strSQL = "";
        Int32 uid = Convert.ToInt32(Request.QueryString["uid"]);
        Int32 pageid = Convert.ToInt32(Request.QueryString["pageid"]);

        string edate = null;
        edate =Convert.ToString(Request.QueryString["edate"]);
        string sdate = null;
        sdate=Convert.ToString(Request.QueryString["sdate"]);
        if (uid==0&&pageid==0&&sdate==null)
            strSQL = "select db_userinfo.username,db_userinfo.lcount,db_userinfo.rcount,db_userinfo.fullname,db_userinfo.password,db_userinfo.contact,db_epins.scheme,db_userinfo.email,date_format(db_userinfo.DOJ,'%d-%m-%y') as doj from db_userinfo,db_epins where db_epins.uid=db_userinfo.uid and db_epins.uid!=0 order by db_userinfo.DOJ desc limit 0,50";
        else if (uid != 0)
            strSQL = "select db_userinfo.username,db_userinfo.lcount,db_userinfo.rcount,db_userinfo.fullname,db_userinfo.password,db_userinfo.contact,db_epins.scheme,db_userinfo.email,date_format(db_userinfo.DOJ,'%d-%m-%y') as doj from db_userinfo,db_epins where db_userinfo.uid<=" + uid + " and db_epins.uid=db_userinfo.uid and db_epins.uid!=0 order by db_userinfo.DOJ desc limit 0,50";
        else if (pageid != 0)
            strSQL = "select db_userinfo.username,db_userinfo.lcount,db_userinfo.rcount,db_userinfo.fullname,db_userinfo.password,db_userinfo.contact,db_epins.scheme,db_userinfo.email,date_format(db_userinfo.DOJ,'%d-%m-%y') as doj from db_userinfo,db_epins where db_epins.uid=db_userinfo.uid and db_epins.uid!=0 order by db_userinfo.DOJ desc limit " + (50 * pageid - 50).ToString() + "," + 50 * pageid + "";
        else if (sdate != null)
            strSQL = "select db_userinfo.username,db_userinfo.lcount,db_userinfo.rcount,db_userinfo.fullname,db_userinfo.password,db_userinfo.contact,db_epins.scheme,db_userinfo.email,date_format(db_userinfo.DOJ,'%d-%m-%y') as doj from db_userinfo,db_epins where (db_userinfo.DOJ<=" + edate + " or db_userinfo.DOJ>=" + sdate + ") and db_epins.uid=db_userinfo.uid and db_epins.uid!=0 order by db_userinfo.DOJ desc limit 0,150";
        
        //Response.Write(strSQL);
        new binder().BindData(strSQL,GridView1);
    }
    protected void page_change(object sender, EventArgs e)
    {
        Response.Redirect("user_info.aspx?pageid="+ddlpage.SelectedValue.ToString());
    }
    protected void date_search(object sender, EventArgs e)
    {
       /* string[] dateofbirth = dob.Split('/');
        dob = dateofbirth[2] + "-" + dateofbirth[0] + "-" + dateofbirth[1];*/
        Response.Redirect("user_info.aspx?sdate=" + txtsdate.Text.Split('/')[2] + "-" + txtsdate.Text.Split('/')[0] + "-" + txtsdate.Text.Split('/')[1] + "&edate=" + txtedate.Text.Split('/')[2] + "-" + txtedate.Text.Split('/')[0] + "-" + txtedate.Text.Split('/')[1]);
    }
    
    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Int32 tmp = 0;
        //List<Int32> child_list = new List<Int32>();
        //*** FilesID ***'
       // int i = 0;
        Label lblid = (Label)e.Row.FindControl("lblid");
        if ((lblid != null))
        {

            lblid.Text = DataBinder.Eval(e.Row.DataItem, "username").ToString();
            /*tmp = Convert.ToInt32(lblid.Text);
            //child_list= chiild.get_child_list(tmp);
            lblid.Text = "life" + tmp.ToString();
            */
        }

        Label lblname = (Label)e.Row.FindControl("lblname");
        if ((lblname != null))
        {

            lblname.Text = DataBinder.Eval(e.Row.DataItem, "fullname").ToString();


        }

        Label lblpass = (Label)e.Row.FindControl("lblpass");
        if ((lblpass != null))
        {

            lblpass.Text = DataBinder.Eval(e.Row.DataItem, "password").ToString();
        }

        Label lblcontact = (Label)e.Row.FindControl("lblcontact");
        if ((lblcontact != null))
        {

            lblcontact.Text = DataBinder.Eval(e.Row.DataItem, "contact").ToString();
        }

        Label lblemail = (Label)e.Row.FindControl("lblemail");
        if ((lblemail != null))
        {

            lblemail.Text = DataBinder.Eval(e.Row.DataItem, "email").ToString();
        }

        Label lbllcount = (Label)e.Row.FindControl("lbllcount");
        if ((lbllcount != null))
        {

            lbllcount.Text = DataBinder.Eval(e.Row.DataItem, "lcount").ToString();
        }
        Label lblrcount = (Label)e.Row.FindControl("lblrcount");
        if ((lblrcount != null))
        {

            lblrcount.Text = DataBinder.Eval(e.Row.DataItem, "rcount").ToString();
        }

        Label lbldoj = (Label)e.Row.FindControl("lbldoj");
        if ((lbldoj != null))
        {

            lbldoj.Text = DataBinder.Eval(e.Row.DataItem, "doj").ToString();
        }

        /*Label lbldirect = (Label)e.Row.FindControl("lbldirect");
        if ((lbldirect != null))
        {
            lbldirect.Text = ob_userinfo.get_down_no(tmp).ToString();
            //lbldirect.Text = DataBinder.Eval(e.Row.DataItem, "cnt").ToString();
        }
        Label lblldown = (Label)e.Row.FindControl("lblldown");
        if ((lblldown != null))
        {
            lblldown.Text = ob_userinfo.get_ldown_no(tmp).ToString();
            //lbldirect.Text = DataBinder.Eval(e.Row.DataItem, "cnt").ToString();
        }
        Label lblrdown = (Label)e.Row.FindControl("lblrdown");
        if ((lblrdown != null))
        {
            lblrdown.Text = ob_userinfo.get_rdown_no(tmp).ToString();
            //lbldirect.Text = DataBinder.Eval(e.Row.DataItem, "cnt").ToString();
        }

        Label lbldown = (Label)e.Row.FindControl("lbldown");
        if ((lbldown != null))
        {
            lbldown.Text = child_list.Count.ToString();
            //lbldirect.Text = DataBinder.Eval(e.Row.DataItem, "cnt").ToString();
        }*/


    }
    protected void delete_user(object sender, EventArgs e)
    {
        ob_userinfo.delete_user(Convert.ToInt32(vars.GetNumberFromStrFaster(txtuser.Text)));
        Response.Redirect("user_info.aspx");

    }
    protected void change_mail(object sender, EventArgs e)
    {
        ob_userinfo.update_user_email(Convert.ToInt32(vars.GetNumberFromStrFaster(txtuserid.Text)), txtnewemail.Text);
        Response.Redirect("user_info.aspx");

    }
    protected void search_user(object sender, EventArgs e)
    {
        //
        Response.Redirect("user_info.aspx?uid=" + Convert.ToInt32(vars.GetNumberFromStrFaster(txtuser.Text)));

    }
}