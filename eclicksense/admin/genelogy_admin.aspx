﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="genelogy_admin.aspx.cs" Inherits="genelogy_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Admin's Geneology View || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
Admin's Geneology View 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">

<h2>
<table width="100%" class="table_class"><tr>
<td class="left">LeftMoney: <asp:Label ID="lbllmony" runat="server" Text=""></asp:Label></td>
    <td class="centeralign">UserId:
    <asp:Label ID="lbluser" runat="server" Text="  "></asp:Label></td>
    <td class="right">Right Money:  
    <asp:Label  ID="lblrmony" runat="server" Text=""></asp:Label></td>
</tr></table></h2>


<asp:Panel id="left" CssClass="centeralign" runat="server">
    <asp:Button id = "but_up" CssClass="upbutton" runat="server" Text="" 
        OnClick="get_up" Height="40px" Width="40px" Visible="false"/>
    <asp:Table ID="Table1" CssClass="centeralign" runat="server" Width="100%" >
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="center" ColumnSpan="8">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/green.jpg" /></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="center" ColumnSpan="8">
        <asp:Label ID="lblroot" runat="server" Text=""></asp:Label>
    </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="center" ColumnSpan="8">
        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/arrow1.gif" />
    </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="1" ID="imgc1" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="2" ID="imgc2" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Label ID="lblc1" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Label ID="lblc2" runat="server" Text=""></asp:Label></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Image ID="Image33" runat="server" ImageUrl="~/images/arrow2.gif" /></asp:TableCell>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Image ID="Image44" runat="server" ImageUrl="~/images/arrow2.gif" /></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="3" ID="imggc3" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="4"  ID="imggc4" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="5"  ID="imggc5" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="6"  ID="imggc6" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc3" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc4" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc5" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc6" runat="server" Text=""></asp:Label></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="2"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/arrow3.gif" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Image ID="Image10" runat="server" ImageUrl="~/images/arrow3.gif" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/arrow3.gif" /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/arrow3.gif" /></asp:TableCell>
    </asp:TableRow>
    
        <asp:TableRow>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="7"  ID="imggcc7" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="8"  ID="imggcc8" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="9"  ID="imggcc9" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="10"  ID="imggcc10" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="11"  ID="imggcc11" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="12"  ID="imggcc12" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="13"  ID="imggcc13" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="14"  ID="imggcc14" runat="server" ImageUrl="~/images/empty.jpg" /></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc7" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc8" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc9" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc10" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc11" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc12" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc13" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="1"><asp:Label ID="lblgcc14" runat="server" Text=""></asp:Label></asp:TableCell>
    </asp:TableRow>
    
    </asp:Table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

<asp:panel id="Panel1" CssClass="sidebarclass" runat="server" groupingtext="Search User  " 
        Font-Bold="True" Font-Size="Large" Width="100%"><br /><br />
    <table>   <tr><td>
    <asp:Label ID="Label2" runat="server" Text="See tree of " Font-Size="Medium"></asp:Label></td>
    
<td><asp:TextBox ID="txtsearch1" CssClass ="searchTextBox" runat="server" Height="100%" width="80%" Font-Size="Medium" Visible="true"></asp:TextBox></td></tr>
<tr><td><asp:TextBox ID="txtsearch" CssClass ="searchTextBox" runat="server" Visible="false"></asp:TextBox></td><td>
    <asp:Label ID="lblid" runat="server" Text="" Font-Size="Medium"></asp:Label></td></tr>
<tr><td></td><td>
    <asp:Button ID="butsearch" CssClass="button" runat="server" Text="Get Geneology" OnClick="search_user" Font-Size="Medium" /></td>
    </table>
    </asp:panel>
    <br>
    <asp:panel id="leftalign" CssClass="sidebarclass" runat="server" groupingtext="User Info  " 
        Font-Bold="True" Font-Size="Medium" Width="100%"><br /><br />
    
    <table cellpadding="5px" cellspacing="10px" width="100%">
    <tr>
    <td>User Id: </td><td>
        <asp:Label ID="lbl_infoid" runat="server" Text=""></asp:Label></td>
    </tr>
        <tr>
    <td>Full Name: </td><td>
        <asp:Label ID="lbl_infofull" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td>Refferer: </td><td>
        <asp:Label ID="lbl_inforef" runat="server" Text=""></asp:Label></td>
    </tr>
            
        <tr>
    <td>Date of Join: </td><td>
        <asp:Label ID="lbl_doj" runat="server" Text=""></asp:Label></td>
    </tr>
    
    </table>
    </asp:panel>
</asp:Content>
