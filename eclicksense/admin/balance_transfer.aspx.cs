﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class balance_transfer : System.Web.UI.Page
{
    int log = 0;
    cs_vars vars = new cs_vars();
    db_payment ob_userinfo = new db_payment();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ob_userinfo.get_var("trasnfer period") != 1)
            butxfer.Enabled = false;
        else
        {
            lblmsg1.Text = "This is not transfer Period";
            butxfer.Enabled = false;
        }

    }
    protected void xferamount(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtamnt.Text))
        {
            double amount = Convert.ToDouble(GetNumberFromStrFaster((txtamnt.Text).ToString()));
            if (!string.IsNullOrEmpty(txtid.Text))
            {
                if (ob_userinfo.checkpin(vars.get_userid(), Convert.ToInt32(GetNumberFromStrFaster(txttransact.Text))))
                {
                    Int16 id = Convert.ToInt16(GetNumberFromStrFaster((txtid.Text).ToString()));

                    if (ddlwallet.SelectedIndex == 0 && amount <= Convert.ToDouble(lblret1.Text))
                    {
                        ob_userinfo.insert_xferred_investor_payment(vars.get_userid(), (amount * (-1)));
                        ob_userinfo.insert_xferred_investor_payment(id, amount);
                        ob_userinfo.send_msg(id, vars.get_userid(), "Userid life" + vars.get_userid().ToString() + " have sent " + amount.ToString() + " to you.");
                        ob_userinfo.send_msg(vars.get_userid(), 0, "You have sent amount " + amount.ToString() + " to life" + id + ".");
                        lblmsg1.Text = "Wallet Transferred";
                    }
                    else if (ddlwallet.SelectedIndex == 1 && amount <= Convert.ToDouble((lblbonus1.Text).ToString()))
                    {
                        ob_userinfo.insert_xferred_royalty_payment(vars.get_userid(), (amount * (-1)));
                        ob_userinfo.insert_xferred_royalty_payment(id, amount);
                        lblmsg1.Text = "Wallet Transferred";
                    }
                    else
                    {
                        lblmsg1.Text = "Not Appropriate";
                        //lblmsg1.Visible = true;
                    }
                }
                else
                {
                    lblmsg1.Text = "Transaction Pin Invalid";
                    //lblmsg1.Visible = true;
                }
                

            }
            else
            {
                lblmsg1.Text = "Not Appropriate";
                //lblmsg1.Visible = true;
            }
            
        }
        else
        {
            lblmsg1.Text = "Not Appropriate";
            //lblmsg1.Visible = true;
        }
        Response.Redirect("balance_transfer.aspx");
    }
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
}