﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="see_code.aspx.cs" Inherits="see_code" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: See Epins & Rpins || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
See E pins and R pins
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
 <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
 onrowdatabound="myGridView_RowDataBound" Font-Size="Medium" GridLines="Both" Width="100%" CellPadding="4" CellSpacing="18" >
  
            <Columns>
            
   <asp:TemplateField>
  
   
    <ItemTemplate>
       <asp:CheckBox id="chksel" runat="server" />
    </ItemTemplate>
    
<HeaderTemplate>
 <input id="chkAll" onclick="javascript:SelectAllCheckboxes(this);" 
              runat="server" type="checkbox" />
    </HeaderTemplate>
   </asp:TemplateField>
   
		<asp:TemplateField HeaderText="Card No">
		<ItemTemplate>
			<asp:Label id="lblid" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
		<asp:TemplateField HeaderText="R code Number">
		<ItemTemplate>
			<asp:Label id="lblno" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
	<asp:TemplateField HeaderText="P code Number">
		<ItemTemplate>
			<asp:Label id="lblecard" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

		<asp:TemplateField HeaderText="User Assigned">
		<ItemTemplate>
			<asp:Label id="lbluser" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Scheme">
		<ItemTemplate>
			<asp:Label id="lblscheme" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	
	    <asp:TemplateField HeaderText="Sold">
		<ItemTemplate>
			<asp:Label id="lblsold" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
    <asp:TemplateField HeaderText="Time">
		<ItemTemplate>
			<asp:Label id="lbltime" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	

	</Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="true" Width="100%" Font-Size=Larger/>
    
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>