﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="precentage.aspx.cs" Inherits="precentage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Change Percentage || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
Change Percentage
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">

<asp:Panel ID="panel1" runat="server" CssClass="panel_main">
 <br>   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
            onrowdatabound="myGridView_RowDataBound" Font-Size="Small" GridLines="Both" Width="75%" CellPadding="20" CellSpacing="18" >
            
            <Columns>
 
			<asp:TemplateField HeaderText="Variable ID">
		<ItemTemplate>
			<asp:Label id="lblid" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
	<asp:TemplateField HeaderText="Variable Name">
		<ItemTemplate>
			<asp:Label id="lblname" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

		<asp:TemplateField HeaderText="Variable Value">
		<ItemTemplate>
			<asp:Label id="lblval" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Last access Time">
		<ItemTemplate>
			<asp:Label id="lbltime" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	

	</Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="true"  Font-Size="Medium"/>
    </asp:GridView>
</asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
<asp:Panel ID="Panel2" runat="server" CssClass="sidebarclass" Font-Size="Medium" Font-Bold="true" GroupingText="Change Variable">
    <table>
    <tr>
    <td>Enter vid</td><td>
        <asp:TextBox ID="txtvid" runat="server"></asp:TextBox></td>
    </tr>
        <tr>
    <td>Enter new Value</td><td>
        <asp:TextBox ID="txtval" runat="server"></asp:TextBox></td>
    </tr>
    <tr><td colspan="3">
        <asp:Button ID="Button1" CssClass="button" runat="server" Text="Change Variable" onclick="change_var" Width="70%"/></td></tr>
    </table>
    </asp:Panel>
</asp:Content>