﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class precentage : System.Web.UI.Page
{
    string strSQL = null;
    cs_vars vars = new cs_vars();
    db_admin ob_userinfo = new db_admin();
    int log = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        string username_c = (string)null;
        string username_s = (string)Session["UserName"];
        HttpCookie cookie = Request.Cookies["UserInfo"];

        if (cookie != null)
        {
            username_c = cookie["UserName"];
            if (username_c != null && (username_s == username_c))
                log = 1;
        }
        //rand = Convert.ToInt32(Request.QueryString["id"]);
        if ( log == 0)
            Response.Redirect("Login.aspx");
        
        strSQL = "select * from db_variables ;";

        new binder().BindData(strSQL,GridView1);
    }
    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //string tmp = "";
        //*** FilesID ***'
        Label lblid = (Label)e.Row.FindControl("lblid");
        if ((lblid != null))
        {
            lblid.Text = DataBinder.Eval(e.Row.DataItem, "vid").ToString();

        }

        Label lblname = (Label)e.Row.FindControl("lblname");
        if ((lblname != null))
        {
            lblname.Text = DataBinder.Eval(e.Row.DataItem, "variable").ToString();

        }

        Label lblval = (Label)e.Row.FindControl("lblval");
        if ((lblval != null))
        {

            lblval.Text = DataBinder.Eval(e.Row.DataItem, "value").ToString();
        }

        Label lbltime = (Label)e.Row.FindControl("lbltime");
        if ((lbltime != null))
        {
            lbltime.Text = DataBinder.Eval(e.Row.DataItem, "time").ToString();
        }

    }
    protected void change_var(object sender, EventArgs e)
    {
        ob_userinfo.update_variable(Convert.ToInt16((txtvid.Text).ToString()), Convert.ToDouble((txtval.Text).ToString()));
        Response.Redirect("precentage.aspx");
    }
}