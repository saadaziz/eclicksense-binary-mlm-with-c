﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class send_money : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "select pid,Notes,uid,date_format( time, '%d %m %Y %k %i' ) AS timing from db_payment where type = 'payment from admin';";
        new binder().BindData(sql, GridView1);
    }
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
    protected void Send_money(object sender, EventArgs e)
    {
        Int32 userid = new db_control().get_suserid_uname(txtuserid.Text);
        new db_admin().insert_payment(userid, Convert.ToInt32(txtamount.Text), "Payment from admin");
        Response.Redirect("send_money.aspx");
    }
    int counter = 0;
    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*string tmp = "";*/
        //counter++;
        Label lblpid = (Label)e.Row.FindControl("lblpid");
        if ((lblpid != null))
        {
            lblpid.Text = DataBinder.Eval(e.Row.DataItem, "pid").ToString();

        }

        Label lblamount = (Label)e.Row.FindControl("lblamount");
        if ((lblamount != null))
        {
            counter += Convert.ToInt32( DataBinder.Eval(e.Row.DataItem, "notes"));
            lblamount.Text = DataBinder.Eval(e.Row.DataItem, "notes").ToString();
        }

        Label lbluser = (Label)e.Row.FindControl("lbluser");
        if ((lbluser != null))
        {

            Int32 userid= Convert.ToInt32( DataBinder.Eval(e.Row.DataItem, "uid"));
            lbluser.Text = new db_control().get_username(userid);
        }

        Label lbltime = (Label)e.Row.FindControl("lbltime");
        if ((lbltime != null))
        {
            //tmp = DataBinder.Eval(e.Row.DataItem, "scheme").ToString();
            lbltime.Text = DataBinder.Eval(e.Row.DataItem, "timing").ToString();
        }

    }
}