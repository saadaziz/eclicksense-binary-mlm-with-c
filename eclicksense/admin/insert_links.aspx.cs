﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class insert_links : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void insert_link(object sender, EventArgs e)
    {
        db_links ldb = new db_links();
        string[] links = System.Text.RegularExpressions.Regex.Split(txtlinks.Text, "\r\n");
        lblmsg.Text= ldb.insert_links(links);

    }
    protected void insert_link_rev(object sender, EventArgs e)
    {
        db_links ldb = new db_links();
        string[] links = System.Text.RegularExpressions.Regex.Split(txtlinks2.Text, "\r\n");
        lblmsg.Text = ldb.insert_links_rev(links);

    }
}