﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="balance_transfer.aspx.cs" Inherits="balance_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Balance Transfer || EclickSense ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
Transfer Balance to other user
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<asp:Panel ID="pnltransfer" CssClass="panel_main" runat="server" >
       <h2>*Please check Amount before transferring and also fill out UserId text.<br />
        <asp:Label ID="lblmsg1" runat="server" Text="" ></asp:Label></h2>
        <br />
    <table class="table_class" width="80%">
<tr>
    <td>Total Return/Investor Income</td>
    <td>
        <asp:Label ID="lblret1" runat="server" Text="0"></asp:Label>
    </td></tr>
    <tr>
    <td>Total Bonus(referral+royalty) Income</td>
    <td>
        <asp:Label ID="lblbonus1" runat="server" Text="0"></asp:Label>
    </td></tr>
    <tr>
    <td>Enter Transaction Pin:</td>
    <td>
        <asp:TextBox ID="txttransact" runat="server"></asp:TextBox>
    </td></tr>
        <tr>
    <td>Enter Amount:</td>
    <td>
        <asp:TextBox ID="txtamnt" runat="server"></asp:TextBox>
    </td></tr>
     <tr>
    <td>Enter UserId:</td>
    <td>
        <asp:TextBox ID="txtid" runat="server"></asp:TextBox>
    </td>
    </tr>
        <tr>
    <td>Enter Wallet</td>
    <td>
        <asp:DropDownList ID="ddlwallet" runat="server">
        <asp:ListItem>From Investor/Return</asp:ListItem>
        <asp:ListItem>From Bonus</asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2" align="right">
        <asp:Button ID="butxfer" CssClass="button" runat="server" Text="Transfer Amount" OnClick= "xferamount" /></td>
    </tr>
    </table>
     <br />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>