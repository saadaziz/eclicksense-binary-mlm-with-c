﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gen_code : System.Web.UI.Page
{
    Int32 epin, ecard;
    static Random _r = new Random();
    db_admin ob_userinfo = new db_admin();
    payment_cs pay_cs = new payment_cs();
    protected void Page_Load(object sender, EventArgs e)
    {
        double amount = 0;

        lbltotepin_admin.Text = (ob_userinfo.get_ecard_no("user")).ToString();
        lbltotepin.Text = (ob_userinfo.get_ecard_no()).ToString();
        lblused.Text = (ob_userinfo.get_ecard_no_used()).ToString();
        List<string> amount_used = ob_userinfo.get_ecard_scheme_used_all();
       
       // int i = 0;
        /*while (amount_used[i] != null)
        {
            amount = amount + pay_cs.get_amount(amount_used[i]);
            i++;
        }*/
        foreach (string scheme in amount_used)
        {
            amount = amount + pay_cs.get_amount(scheme);
        }
        lblamount_used.Text = amount.ToString();
        //i = 0;
        amount = 0;
        amount_used = ob_userinfo.get_ecard_scheme_all();
        
       /* while (amount_used[i] != null)
        {
            amount = amount + pay_cs.get_amount(amount_used[i]);
            i++;
        }*/
        foreach (string scheme in amount_used)
        {
            amount = amount + pay_cs.get_amount(scheme);
        }
        lblamount_gen.Text = amount.ToString();

         
    }

    protected void gen_ecard(object sender, EventArgs e)
    {
        cs_vars vars = new cs_vars();
        if (txtquantity.Text != "0")
        {
            for (int i = 0; i < Convert.ToInt16(txtquantity.Text); i++)
                while (true)
                {
                    epin = _r.Next(99999);
                    ecard = _r.Next(999999999);
                    if (!ob_userinfo.checkepin(epin, ecard, 0))
                    {

                        //Panel_epin.Visible = true;
                        string epin_str = (DDLamout.SelectedIndex + 1).ToString() + 5.ToString() + epin.ToString();
                        string scheme = vars.get_scheme_no(DDLamout.SelectedIndex + 1, 2);
                        
                        ob_userinfo.insert_epins_byadmin(Convert.ToInt32(epin_str), ecard, scheme, 0);
                        break;
                    }

                }
        }

    }
}