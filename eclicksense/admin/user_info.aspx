﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin/mst_admin.master" CodeFile="user_info.aspx.cs" Inherits="user_info" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: User Information || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
 User Information 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<asp:Panel ID="panel3" runat="server">
<table>
<tr>
<td>Delete/Search user</td>
<td>
    <asp:TextBox ID="txtuser" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td><asp:Button ID="Button2" CssClass="button" runat="server" Text="Search User" OnClick="search_user" /></td><td>
   <!-- <asp:Button ID="Button1" CssClass="button" runat="server" Text="Delete User" OnClick="delete_user" /></td>--></tr>
</table><br />
<table>
<tr>
<td>Userid</td>
<td>
    <asp:TextBox ID="txtuserid" runat="server"></asp:TextBox></td></tr><tr><td>Email Address</td><td><asp:TextBox ID="txtnewemail" runat="server"></asp:TextBox></td>
</tr>
<tr><td>
    <asp:Button ID="Button4" CssClass="button" runat="server" Text="Cange email" OnClick="change_mail" /></td></tr>
</table>
</asp:Panel>
<br />
<asp:Panel ID="panel2" runat="server">
Page Number::<asp:DropDownList ID="ddlpage" runat="server" OnSelectedIndexChanged="page_change" AutoPostBack="true">
    </asp:DropDownList>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            onrowdatabound="myGridView_RowDataBound" Font-Size="Small" GridLines="Horizontal"  Width="200%" CellPadding="10" CellSpacing="2" >
            
            <Columns>
 
		<asp:TemplateField HeaderText="User Id">
		<ItemTemplate>
			<asp:Label id="lblid" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
	<asp:TemplateField HeaderText="User Name">
		<ItemTemplate>
			<asp:Label id="lblname" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

		<asp:TemplateField HeaderText="Password">
		<ItemTemplate>
			<asp:Label id="lblpass" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Contact">
		<ItemTemplate>
			<asp:Label id="lblcontact" runat="server" Text="Contact not available"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	
	
		    <asp:TemplateField HeaderText="Email">
		<ItemTemplate>
			<asp:Label id="lblemail" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
	    <asp:TemplateField HeaderText="lcount">
		<ItemTemplate>
			<asp:Label id="lbllcount" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
     <asp:TemplateField HeaderText="rcount">
		<ItemTemplate>
			<asp:Label id="lblrcount" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

   <asp:TemplateField HeaderText="DOJ">
		<ItemTemplate>
			<asp:Label id="lbldoj" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	
	  
	</Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="true" Width="100%" Font-Size="Larger"/>
    </asp:GridView>
</asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
<h2>Search by Date</h2>
<asp:ScriptManager ID="ScriptManager1" runat="server" />
    Start Date<asp:TextBox ID="txtsdate" runat="server" Width="150px" Height="25px"></asp:TextBox>  
     <br />
    <asp:CalendarExtender   
        ID="CalendarExtender1"   
        TargetControlID="txtsdate"   
        runat="server" />
        End Date <asp:TextBox ID="txtedate" runat="server" Width="150px" Height="25px"></asp:TextBox>  
        <asp:CalendarExtender   
        ID="CalendarExtender2"   
        TargetControlID="txtedate"   
        runat="server" />
        <br />
    <asp:Button CssClass="button" ID="butdate" runat="server" Text="Search" OnClick="date_search" />
</asp:Content>