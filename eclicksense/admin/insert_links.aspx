﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="~/admin/insert_links.aspx.cs" Inherits="insert_links" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Insert Links || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
Insert Links
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<asp:Panel ID="Panel3" CssClass="panel_main" runat="server">

    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    <h2>Non Money generating links:</h2>
    <table class="pattern-style-b"  cellpadding="5px" cellspacing="8px" width="100%"><tr>
    <td>Insert Links</td>
    <td>
        <asp:TextBox ID="txtlinks" runat="server" TextMode="MultiLine" Rows="5" Columns="10" Width="90%" ></asp:TextBox></td>
    </tr>
    <tr><td><asp:Button ID="Button4" CssClass="button" runat="server" Text="Insert Links" OnClick="insert_link"/></td></tr></table>
    <br />
    <h2>Money generating links:</h2>
        <table class="pattern-style-b"  cellpadding="5px" cellspacing="8px" width="100%"><tr>
    <td>Insert Links</td>
    <td>
        <asp:TextBox ID="txtlinks2" runat="server" TextMode="MultiLine" Rows="5" Columns="10" Width="90%" ></asp:TextBox></td>
    </tr>
    <tr><td><asp:Button ID="Button1" CssClass="button" runat="server" Text="Insert Links" OnClick="insert_link_rev"/></td></tr></table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>