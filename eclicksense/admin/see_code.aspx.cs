﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class see_code : System.Web.UI.Page
{
    binder ob_bind = new binder();

    string strSQL = null;
    db_admin ob_userinfo = new db_admin();
    cs_vars vars = new cs_vars();
    protected void Page_Load(object sender, EventArgs e)
    {
        strSQL = "select eid,epin_no,ecard_no,db_epins.uid,scheme,sold,db_userinfo.fullname as fulln, date_format(generation_time,'%d-%m-%Y') as time1 from db_epins,db_userinfo where db_epins.uid=db_userinfo.uid order by generation_time desc limit 0,100 ;";
        ob_bind.BindData(strSQL, GridView1);

    }

    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //string tmp = "";

        Label lblid = (Label)e.Row.FindControl("lblid");
        if ((lblid != null))
        {

            lblid.Text = DataBinder.Eval(e.Row.DataItem, "eid").ToString();
            //count++;
            //tmp = lblid.Text;
        }
        //*** FilesID ***'
        Label lblno = (Label)e.Row.FindControl("lblno");
        if ((lblno != null))
        {

            lblno.Text = DataBinder.Eval(e.Row.DataItem, "epin_no").ToString();

            //tmp = lblid.Text;
        }

        Label lblecard = (Label)e.Row.FindControl("lblecard");
        if ((lblecard != null))
        {

            lblecard.Text = DataBinder.Eval(e.Row.DataItem, "ecard_no").ToString();
            //lblecard.Text = ob_userinfo.get_fullname(Convert.ToInt32((lblid.Text).ToString()));
        }

        Label lbluser = (Label)e.Row.FindControl("lbluser");
        if ((lbluser != null))
        {

            lbluser.Text = (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fulln")));
            //lbluser.Text = "sda";
        }

        Label lblscheme = (Label)e.Row.FindControl("lblscheme");
        if ((lblscheme != null))
        {
            lblscheme.Text = vars.get_scheme_name(DataBinder.Eval(e.Row.DataItem, "scheme").ToString());
        }

        Label lblsold = (Label)e.Row.FindControl("lblsold");
        if ((lblsold != null))
        {
            lblsold.Text = DataBinder.Eval(e.Row.DataItem, "sold").ToString();
        }

        Label lbltime = (Label)e.Row.FindControl("lbltime");
        if ((lbltime != null))
        {
            lbltime.Text = DataBinder.Eval(e.Row.DataItem, "time1").ToString();
        }

    }
}