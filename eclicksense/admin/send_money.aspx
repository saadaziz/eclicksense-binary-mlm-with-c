﻿<%@ Page Language="C#" MasterPageFile="~/admin/mst_admin.master" AutoEventWireup="true" CodeFile="send_money.aspx.cs" Inherits="send_money" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Send Moeny to User || Lifeforce BD ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader1" Runat="Server">
Please be careful about the amount and User Id
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<table class="table_class">
    <tr><td>Enter Userid</td>
    <td>
        <asp:TextBox ID="txtuserid" runat="server"></asp:TextBox></td></tr>
    <tr><td>Enter Amount</td>
    <td>
    <asp:TextBox ID="txtamount" runat="server"></asp:TextBox></td></tr>
    <tr><td colspan="2" align="right">
            <asp:Button ID="butsend" runat="server" CssClass="button" Text="Send Notes" OnClick="Send_money"  /></td></tr>
        </table>
        
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="gridview"
            onrowdatabound="myGridView_RowDataBound" Font-Size="Small" GridLines="Both" Width="100%" CellPadding="5" CellSpacing="8" >
            
            <Columns>
            <asp:TemplateField HeaderText="Payment ID">
		<ItemTemplate>
			<asp:Label id="lblpid" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

 	<asp:TemplateField HeaderText="Amount">
		<ItemTemplate>
			<asp:Label id="lblamount" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="To User">
		<ItemTemplate>
			<asp:Label id="lbluser" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	
     <asp:TemplateField HeaderText="Time">
		<ItemTemplate>
			<asp:Label id="lbltime" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>
	</Columns>
    
            <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="true" Width="100%" Font-Size="Medium"/>
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>
