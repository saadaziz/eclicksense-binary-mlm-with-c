﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class New_registration : System.Web.UI.Page
{
    string IP = null;
    db_control ob_userinfo = new db_control();
    cs_vars vars = new cs_vars();
    string uname = null;

    List<Int16> parents = new List<Int16>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ob_userinfo.get_var("registration period") == 0)
        {
            Button1.Enabled = false;
            lblmsg.Text = "Registration period is off.";
        }
    }
    protected void reg_Click(object sender, EventArgs e)
    {
         //Page.ClientScript.RegisterStartupScript(this.GetType(), "showww1", "<script type='text/javascript'>showPopUp1('dialog1')</script>");
        string username = txtusername.Text.Trim();
        string full = "New User(change after login)";
        string email = txtemail.Text;
        string pin = txtpin1.Text;
        string code1 = txtpin2.Text;
        string contact = "+8800000000000";
        string newdob = "4/10/1988";
        string address = "dhaka";
        string newpass = txtpassword.Text;
        string zip = "1001";
        if (full != null && email != null && pin != null && username != null && newpass != null && code1 != null && address != null && zip != null && contact != null)
            if (isEmail(email)  && (!email.Contains("@ovi.com")))
                /*  if (!ob_userinfo.get_phone(Convert.ToInt32(GetNumberFromStrFaster(txtcontact.Text))))
                     */
                if (!username.Contains("admin") && (!username.Contains("fran")) && (!ob_userinfo.checkuser(username)))
                {
                    if (ob_userinfo.checkepin(Convert.ToInt32(GetNumberFromStrFaster(pin)), Convert.ToInt32(GetNumberFromStrFaster(code1)), 0))
                    {
                        // string userEnteredCode = txtbot.Text;

                        if (txtpassword.Text.Length>5)
                        {
                            ref_click(newpass, username, address, email, newdob, full, contact, pin, code1);

                        }
                        else
                            lblmsg.Text = "Passsword Must be bigger than 5 characters";
                    }
                    else
                        lblmsg.Text = "Pin doesn't exist";
                }
                 else
                 {
                     lblmsg.Text = "Username not valid, Already exists or prohibited";
                     //Response.Write(GetNumberFromStrFaster(txtcontact.Text));
                 }
                else
                    lblmsg.Text = "Email not valid or currently active. If mail address is contains xxxx@Ovi.com please change it.";
        else
            lblmsg.Text = "Please enter all fields";

    }
    private bool ValidateUserCode(string userEnteredCode)
    {
        bool flag = false;
        if (Session["Code"].ToString().Equals(userEnteredCode))
        {
            flag = true;
        }
        else
        {
            // clear the session and generate a new code 
            Session["Code"] = null;
            flag = false;
            lblmsg.Text = "Please enter the Image code correctly";
        }

        return flag;
    }
    protected void ref_click(string newpass, string username, string address, string email, string newdob, string full, string contact, string pin, string card)
    {
        string tmp = name_attendee_1.Text;
        payment_cs pay_cs = new payment_cs();
        if ((tmp != null))
        {
            Int16 numbers = ob_userinfo.get_suserid_uname(tmp);

            Int16 log = numbers;
        
            if (log!=0)
            {
                ob_userinfo.insertuser(newpass, address, email, newdob, full, contact, username);

                ob_userinfo.insert_uid_into_epin(email, Convert.ToInt32(pin), Convert.ToInt32(card));

                int id = ob_userinfo.get_suserid_uname(username);

                //Session.Add("eclicksense", Convert.ToString(id));

                string scheme1 = ob_userinfo.get_scheme(id);
                pay_cs.refarral_inc(log, scheme1);
                 /*pay_cs.bid_inc(vars.get_userid());*/
                //ob_userinfo.insert_login(id, GetIP());
                string hand_tmp = "";
                if (num_attendees.Checked == true)
                    hand_tmp = "L";
                else
                    hand_tmp = "R";
                ob_userinfo.insert_sponsor(id, log, hand_tmp);
                // ob_userinfo.update_cat_uid(ddlcatagory.SelectedItem.Text.ToString(), id);
                DateTime dt = DateTime.Now;

                if (scheme1.Equals("x"))
                {
                    send_email email_ob = new send_email();
                    vars.set_root(id);
                    string cmd = "Dear " + full + ", \n" + " We are Declaring you as our honorable member. \n" + "\n  " +
                    "\n\tFull Name: " + full +
                    ",\n\t Username: " +
                      username +
                     "\n\t Password(which you can change later):" +
                     newpass +
                     "\n\t Referral Id: " + tmp +
                     "\n\n N.B.: You can change your password later from the settings page after login. \n" +
                     "For any technical support, write your inquiry: info@eclicksense.com " +
                     "\nBe with us, we are for changing the world." +
                    "\nThanks & Regards," +
                    "\n Eclicksource.com Management. ";
                    string mail_send = email_ob.s_email_reg(email, cmd);
                    cmd = cmd + "\n" + mail_send;
                    //string tmp1 = ob_userinfo.checkside(numbers);
                    if (hand_tmp == "L")
                    {
                        while (numbers != 0)
                        {
                            numbers = ob_userinfo.get_most(numbers, "L");
                            if (numbers != 0)
                                log = numbers;
                        }
                    }
                    else if (hand_tmp == "R")
                    {
                        while (numbers != 0)
                        {
                            numbers = ob_userinfo.get_most(numbers, "R");
                            if (numbers != 0)
                                log = numbers;
                        }
                    }
                    //lblmsg.Text = "Only Left Side is available";
                    int i = 0;

                    if (log != 0)
                    {
                        //pay_cs.refarral_inc(log, scheme1);
                        ob_userinfo.insert_reference(id, log, hand_tmp);
                        if (!scheme1.Equals("x"))
                        {
                            pay_cs.royalty_inc(id, scheme1);

                        }
                        while (true)
                        {
                            parents.Add(ob_userinfo.get_parent(id));

                            if (parents[i] == 0) break;
                            id = parents[i];
                            i++;
                        }
                        for (int j = 0; j <= i; j++)
                        {
                            if (!scheme1.Equals("x"))
                                pay_cs.royalty_inc(parents[j], scheme1);
                        }
                    }
                    else if (!scheme1.Equals("x"))
                        pay_cs.royalty_main(parents, scheme1);
                    uname = "eclick" + id.ToString();
                    HttpCookie cookie = new HttpCookie("UserInfo");
                    cookie.Expires = DateTime.Now.AddHours(1);
                    cookie["UserName"] = uname;
                    // query check1 = new query();
                    // cookie["UserId"] = check1.GetUserID(uname, pass);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);

                    Session["UserName"] = uname;
                    lblsuccess.Text = cmd;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showww1", "<script type='text/javascript'>showPopUp1('dialog1')</script>");
                    // Response.Redirect("my_referrals.aspx");
                }
            }
            else
                lblmsg.Text = "Sorry, No user found in this Username. Check your Sponsor Username Again";
        }
        else
            lblmsg.Text = "Please Fill the form correctly.";

        /*       else
                   Response.Redirect("default.aspx");*/
    }
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
    protected string GetIP()
    {
        string strHostName = "";
        strHostName = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strHostName == null)

            strHostName = Request.ServerVariables["REMOTE_ADDR"];

        return strHostName;
        //return Dns.GetHostName();

    }

    public static bool isEmail(string inputEmail)
    {
        //inputEmail = NulltoString(inputEmail);
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return (true);
        else
            return (false);
    }
    

}