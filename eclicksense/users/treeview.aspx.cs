﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class treeview : System.Web.UI.Page
{
    //int log = 0;

    db_control ob_gg = new db_control();
    cs_vars vars = new cs_vars();
    gen_db ob_db = new gen_db();

    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hplgenology");
        hpl.CssClass = "active";
        if (!IsPostBack)
        {
            Int32 userid = vars.get_userid();
            TreeNode treeNode = new TreeNode();

            treeNode.Text = ob_gg.get_username(vars.get_userid());
            Int32 luserid = ob_db.get_lchild(userid);
            Int32 ruserid = ob_db.get_rchild(userid);
            if (luserid != 0)
            {
                TreeNode ltreeNode = new TreeNode();
                ltreeNode.Text = ob_gg.get_username(luserid);
                ltreeNode.ToolTip = "Left Hand";
                treeNode.ChildNodes.Add(ltreeNode);
            }
            if (ruserid != 0)
            { 
                TreeNode rtreeNode = new TreeNode();
                rtreeNode.Text = ob_gg.get_username(ruserid);
                rtreeNode.ToolTip = "Right Hand";
                treeNode.ChildNodes.Add(rtreeNode);
            }
            

            TreeView1.Nodes.Add(treeNode);
            db_control ob_userinfo = new db_control();
            Int32 sponid = (ob_userinfo.get_sponid(userid));
            if (sponid == -1)
                lblsponsor.Text = "Root";
            else
            {
                lblsponsor.Text = ob_gg.get_username(sponid).ToString();



                lblsponsorname.Text = (ob_userinfo.get_uname(Convert.ToInt32(sponid))).ToString();

                lblsponHand.Text = ob_userinfo.get_hand(vars.get_userid(), "db_sponsors");
            }
        }
        
       
    }
    protected void nodeselect(object sender, EventArgs e)
    {
       
        System.Threading.Thread.Sleep(500);
        //this.ClientScript.RegisterStartupScript(this.GetType(), "showww", "<script type='text/javascript'>showPopUp('dialog')</script>");
        TreeNode currentNode = TreeView1.SelectedNode;
        if (currentNode.ChildNodes.Count == 0)
        {
            string userid = currentNode.Text;
            Int32 userid_int =Convert.ToInt32( ob_gg.get_suserid_uname(userid));

            Int32 luserid = ob_db.get_lchild(userid_int);
            Int32 ruserid = ob_db.get_rchild(userid_int);
            if (luserid != 0)
            {
                TreeNode ltreeNode = new TreeNode();
                ltreeNode.ToolTip = "Left Hand";

                ltreeNode.Text = ob_gg.get_username(luserid);
                currentNode.ChildNodes.Add(ltreeNode);
            }
            if (ruserid != 0)
            {
                TreeNode rtreeNode = new TreeNode();

                rtreeNode.Text = ob_gg.get_username(ruserid);
                rtreeNode.ToolTip = "Right Hand";

                currentNode.ChildNodes.Add(rtreeNode);
            }
            db_control ob_userinfo = new db_control();
            Int32 sponid = (ob_userinfo.get_sponid(userid_int));
            if (sponid == -1)
                lblsponsor.Text = "Root";
            else
            {
                lblsponsor.Text = ob_gg.get_username(sponid).ToString();

                lblsponsorname.Text = (ob_userinfo.get_uname(Convert.ToInt32(sponid))).ToString();

                lblsponHand.Text = ob_userinfo.get_hand(vars.get_userid(), "db_sponsors");
            }
        }
    }
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
}