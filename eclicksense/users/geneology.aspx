﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master" AutoEventWireup="true" CodeFile="geneology.aspx.cs" Inherits="users_geneology" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Geneology || EclickSense.com ::.</title>
<style>
.watermark
{
	background: #FFAAFF;
}

.popupControl
{
    border: 2px solid Black;
    background-color: #CCCCFF;
    position: absolute;
    visibility: hidden;
    font-family: Arial;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Geneology
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server" />
 <asp:Panel ID="pnlupdate1" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress1"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopDOJ" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1"
     runat="server"
     BehaviorID="pce1"   CommitProperty="value"
                 PopupControlID="pnlupdate1" 
                Position="Bottom"
                TargetControlID="imgc1" >
            </ajaxToolkit:PopupControlExtender>  
<asp:Panel ID="pnlupdate2" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress2"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopdoj2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc2" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2"
     runat="server"
     BehaviorID="pce2"   CommitProperty="value"
                 PopupControlID="pnlupdate2" 
                Position="Bottom"
                TargetControlID="imgc2" >
            </ajaxToolkit:PopupControlExtender>  
            <asp:Panel ID="pnlupdate3" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress3"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopdoj3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc3" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender3"
     runat="server"
     BehaviorID="pce3"   CommitProperty="value"
                 PopupControlID="pnlupdate3" 
                Position="Bottom"
                TargetControlID="imggc3" >
            </ajaxToolkit:PopupControlExtender>  
            <asp:Panel ID="pnlupdate4" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress4"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopdoj4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc4" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender4"
     runat="server"
     BehaviorID="pce4"   CommitProperty="value"
                 PopupControlID="pnlupdate4" 
                Position="Bottom"
                TargetControlID="imggc4" >
            </ajaxToolkit:PopupControlExtender>  
            <asp:Panel ID="pnlupdate5" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress5"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopdoj5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc5" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender5"
     runat="server"
     BehaviorID="pce5"   CommitProperty="value"
                 PopupControlID="pnlupdate5" 
                Position="Bottom"
                TargetControlID="imggc5" >
            </ajaxToolkit:PopupControlExtender>  
            <asp:Panel ID="pnlupdate6" runat="server" CssClass="popupControl">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                 
                    <ContentTemplate>
                    <asp:updateprogress id="UpdateProgress6"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">
                           <img src="ajax-loader.gif" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <table><tr>
                    <td>Username</td><td>
                        <asp:Label ID="lblpopuname6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Fullname</td><td>
                        <asp:Label ID="lblpopfname6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                     <tr>
                    <td>Address</td><td>
                        <asp:Label ID="lblpopaddr6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Date Of Join</td><td>
                        <asp:Label ID="lblpopdoj6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Left Team:</td><td>
                        <asp:Label ID="lblpoplc6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    <tr>
                    <td>Right Team:</td><td>
                        <asp:Label ID="lblpoprc6" runat="server" Text=""></asp:Label></td>    
                    </tr>
                    </table>
                    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Panel>
  <ajaxToolkit:PopupControlExtender ID="PopupControlExtender6"
     runat="server"
     BehaviorID="pce6"   CommitProperty="value"
                 PopupControlID="pnlupdate6" 
                Position="Bottom"
                TargetControlID="imggc6" >
            </ajaxToolkit:PopupControlExtender>  
                        
        



 <asp:Label ID="lblulti" runat="server" Text="You are not allowed to refer and increase your geneology. Thanks for being with us." Visible="false"></asp:Label>
    <asp:Panel ID="pnl_ulti" runat="server">
    
<h2>
</h2>


<asp:Panel id="left" CssClass="centeralign" runat="server">
    <asp:Button id = "but_up" CssClass="upbutton" runat="server" Text="Go UP" 
        OnClick="get_up" Height="60px" Width="60px"/>
    <asp:Table ID="Table1" CssClass="centeralign" runat="server" Width="50%" >
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="center" ColumnSpan="5">
        <asp:Image ID="Image1" runat="server" ImageUrl="images/green.png" /></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="center" ColumnSpan="8">
        <asp:Label ID="lblroot" runat="server" Text=""></asp:Label>
    </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow Width="130%">
    <asp:TableCell HorizontalAlign="center" ColumnSpan="8">
        <asp:Image ID="Image2" runat="server" ImageUrl="images/arrow1.png" Width="100%" />
    </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="7" HorizontalAlign="left"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="1" ID="imgc1" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce1').showPopup();" onMouseOut="$find('pce1').hidePopup();"  /></asp:TableCell>
    <asp:TableCell ColumnSpan="7" HorizontalAlign="Center"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="2" ID="imgc2" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce2').showPopup();" onMouseOut="$find('pce2').hidePopup();"  /></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
    <asp:TableCell ColumnSpan="7" HorizontalAlign="left">
        <asp:Label ID="lblc1" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="7" HorizontalAlign="Center">
        <asp:Label ID="lblc2" runat="server" Text=""></asp:Label></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Image ID="Image33" runat="server" ImageUrl="images/arrow2.png" /></asp:TableCell>
    <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
        <asp:Image ID="Image44" runat="server" ImageUrl="images/arrow2.png" /></asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="3" ID="imggc3" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce3').showPopup();" onMouseOut="$find('pce3').hidePopup();"  /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="4"  ID="imggc4" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce4').showPopup();" onMouseOut="$find('pce4').hidePopup();"  /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="5"  ID="imggc5" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce5').showPopup();" onMouseOut="$find('pce5').hidePopup();"  /></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:ImageButton OnCommand="set_sidebar" CommandArgument="6"  ID="imggc6" runat="server" ImageUrl="images/empty.png" onMouseOver="$find('pce6').showPopup();" onMouseOut="$find('pce6').hidePopup();"  /></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc3" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc4" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc5" runat="server" Text=""></asp:Label></asp:TableCell>
    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblgc6" runat="server" Text=""></asp:Label></asp:TableCell>
    </asp:TableRow>
    
 
    
    </asp:Table>
    </asp:Panel>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
<!--
<asp:panel id="Panel1" CssClass="sidebarclass" runat="server" groupingtext="Search User  " 
        Font-Bold="True" Font-Size="Large" Width="100%"><br />
    <table>   <tr><td>
    <asp:Label ID="Label1" runat="server" Text="See tree of " Font-Size="Medium"></asp:Label></td><td>
    <asp:TextBox ID="txtsearch" CssClass ="searchTextBox" runat="server" Height="100%" width="80%" Font-Size="Medium"></asp:TextBox>
</td></tr><tr><td></td><td>
    <asp:Label ID="lblid" runat="server" Font-Size="Medium"></asp:Label></td></tr>
<tr><td></td><td>
    <asp:Button ID="butsearch" CssClass="button" runat="server" Text="Get Geneology"  Font-Size="Medium" /></td>
    </table>
    </asp:panel>
    -->
    <table width="80%"><tr>
    <td >UserId:</td><td>
    <asp:Label ID="lbluser" runat="server" Text="  "></asp:Label></td>
    
</tr>
<tr><td>Todays Left Royalty:</td><td>
    <asp:Label ID="lbllmony" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Todays Right Royalty:</td><td>
    <asp:Label ID="lblrmony" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Total Left Royalty:</td><td>
    <asp:Label ID="lbllall" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Total Right Royalty:</td><td>
    <asp:Label ID="lblrall" runat="server" Text=""></asp:Label></td></tr>
    </table>

    <h2>User Information</h2>
    <asp:panel id="pnl2" CssClass="sidebarclass"  runat="server" Width="100%">
    
        <table class="table_cls" width="100%">
        <tr>
        <td>User Id: </td><td>
            <asp:Label ID="lbl_infoid" runat="server" Text=""></asp:Label></td>
        </tr>
            <tr>
        <td>Full Name: </td><td>
            <asp:Label ID="lbl_infofull" runat="server" Text=""></asp:Label></td>
        </tr>
       <!--<tr>
        <td>Sponsor Id: </td><td>
            <asp:Label ID="lbl_inforef" runat="server" Text=""></asp:Label></td>
        </tr>           -->
        <tr>
        <td>DOJ: </td><td>
            <asp:Label ID="lbl_doj" runat="server" Text=""></asp:Label></td>
        </tr>
    
        </table>
    
        <!--<table class="table_cls" width="100%">
             <tr>
            <td>In Left Team: </td><td>
                <asp:Label ID="lblleft" runat="server" Text=""></asp:Label></td>
            </tr>
             <tr>
            <td>In Right Team: </td><td>
                <asp:Label ID="lblright" runat="server" Text=""></asp:Label></td>
            </tr>
            </table>-->
        </asp:Panel>
        <br /><br />

</asp:Content>