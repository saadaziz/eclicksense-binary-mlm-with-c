﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master" AutoEventWireup="true" CodeFile="editProfile.aspx.cs" Inherits="users_editProfile" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Edit Profile || EclickSense ::.</title>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
  <!--[if IE 7]>
    <style type="text/css">
        #vtab > ul > li.selected{
            border-right: 1px solid #fff !important;
        }
        #vtab > ul > li {
            border-right: 1px solid #ddd !important;
        }
        #vtab > div { 
            z-index: -1 !important;
            left:1px;
        }
    </style>
    <![endif]-->
    <style type="text/css">
        
       
    </style>

    <script type="text/javascript">
        $(function () {
            var $items = $('#vtab>ul>li');
            $items.mouseover(function () {
                $items.removeClass('selected');
                $(this).addClass('selected');

                var index = $items.index($(this));
                $('#vtab>div').hide().eq(index).show();
            }).eq(1).mouseover();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Edit Profile
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
<div id="vtab">
        <ul>
            <li class="home selected"></li>
            <li class="login"></li>
            <li class="support"></li>
             <li class="email"></li>
        </ul>
        <div>
            <h4>Current Information</h4>
				<div>
    <table class="table_class" width="100%">
    <tr>
        <td>User Id: </td><td>
        <asp:Label ID="lbl_infoid" runat="server" Text=""></asp:Label></td>
    </tr>
        <tr>
        <td>Full Name: </td><td>
        <asp:Label ID="lbl_infofull" runat="server" Text=""></asp:Label></td>
    </tr>
    
        <tr>
        <td>Address: </td><td>
        <asp:Label ID="lbl_addr" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td>Email: </td><td>
        <asp:Label ID="lbl_email" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td>Contact: </td><td>
        <asp:Label ID="lbl_contact" runat="server" Text=""></asp:Label></td>
    </tr>
   
    

    </table></div>
        </div>
        <div>
            <h4>
                Contact Edit</h4>
            <table class="table_class" width="100%">
                    <tr>
                    <td>Contact No:</td><td>
                        <asp:TextBox ID="txtnewcontact" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                    <td></td><td>
                        <asp:Button CssClass="button" ID="Button3" runat="server" Text="Change" OnClick="change_contact"/></td></tr>
                </table>
                <br />
                  <h4>
                Change FullName</h4>
                <table class="table_class" width="100%">
                    <tr>
                    <td>Full Name</td><td>
                        <asp:TextBox ID="txtnewname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                    <td></td><td>
                        <asp:Button CssClass="button" ID="Button2" runat="server" Text="Change" OnClick="change_full" /></td></tr>
                </table>
        </div>
        <div>
            <h4>
                Password Change</h4>
                 <table class="table_class">
            <tr><td>Old Password</td>
            <td>
                <asp:TextBox ID="txtold" TextMode="Password" runat="server"></asp:TextBox></td></tr>
            <tr><td>New Password</td>
            <td>
                <asp:TextBox ID="txtnewpass" TextMode="Password" runat="server"></asp:TextBox></td>
            </tr>
            <tr><td>Retype New Password</td>
            <td>
                <asp:TextBox ID="txtrenew" TextMode="Password" runat="server"></asp:TextBox></td>
            </tr>
            <tr><td></td><td align="right">
                <asp:Button CssClass="button" ID="Button4" runat="server" Text="Change" OnClick="change_pass"/></td></tr>
                            </table>
        </div>
        <div>
            <h4>
                Email Change</h4>
                 <table class="table_class">
            <tr><td>Email Address</td>
            <td>
                <asp:TextBox ID="txtemail"  runat="server"></asp:TextBox></td></tr>
            
            <tr><td></td><td align="right">
                <asp:Button CssClass="button" ID="Button1" runat="server" Text="Change" OnClick="change_email"/></td></tr>
                            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>