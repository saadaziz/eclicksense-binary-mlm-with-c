﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class gen_pins : System.Web.UI.Page
{
    cs_vars vars = new cs_vars();
    static Random _r = new Random();
    string strSQL = "";

    db_control ob_userinfo = new db_control();
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hplgenpins");
        hpl.CssClass = "active";
        string username1 = ob_userinfo.get_username(vars.get_userid());
        Int32 counter = ob_userinfo.get_count_epin(username1);
        if (ob_userinfo.get_var("generate pin") != 1)
        {
            but_gen.Enabled = false;
            // but_xl.Enabled = false;
        }
        lblbalance.Text = ob_userinfo.get_total_income(vars.get_userid()).ToString();

        strSQL = "select eid,epin_no,ecard_no,uid from db_epins where created_by = '" + username1 + "' order by generation_time desc;";
        new binder().BindData(strSQL, GridView1);
        lblmsg.Text = "You have generated " + counter.ToString() + " pins.";
        GridView1.Columns[0].Visible = false;
        string show = Convert.ToString(Request.QueryString["show"]);
        if (show != null)
        {
            strSQL = "select eid,epin_no,ecard_no,uid from db_epins where created_by = '" + username1 + "' and uid=0 order by generation_time desc;";
            new binder().BindData(strSQL, GridView1);

            GridView1.Columns[3].Visible = false;
            CheckBox1.Checked = true;
        }

    }
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string tmp = "";
        //counter++;
        Label lbleid = (Label)e.Row.FindControl("lbleid");
        if ((lbleid != null))
        {
            lbleid.Text = DataBinder.Eval(e.Row.DataItem, "eid").ToString();

        }

        Label lblepin = (Label)e.Row.FindControl("lblepin");
        if ((lblepin != null))
        {
            lblepin.Text = DataBinder.Eval(e.Row.DataItem, "epin_no").ToString();

        }

        Label lblecode = (Label)e.Row.FindControl("lblecode");
        if ((lblecode != null))
        {

            lblecode.Text = DataBinder.Eval(e.Row.DataItem, "ecard_no").ToString();

        }

        Label lbluser = (Label)e.Row.FindControl("lbluser");
        if ((lbluser != null))
        {
            //tmp = DataBinder.Eval(e.Row.DataItem, "scheme").ToString();
            tmp = DataBinder.Eval(e.Row.DataItem, "uid").ToString();
            if (tmp == "0")
                lbluser.Text = tmp;
            else
                lbluser.Text = ob_userinfo.get_username(Convert.ToInt32(GetNumberFromStrFaster(tmp)));
        }

    }
    protected void gen_excel(object sender, EventArgs e)
    {
        new binder().create_xls("select eid,epin_no,CONCAT('\"',ecard_no,'\"') as rpin,uid from db_epins where created_by = 'eclick" + vars.get_userid().ToString() + "'; ");
        string filePath = System.Web.HttpContext.Current.Server.MapPath("pdfs1") + "\\MyExcelFile" + vars.get_userid().ToString() + ".xls";

        if (!File.Exists(filePath))
            return;

        var fileInfo = new System.IO.FileInfo(filePath);
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", filePath));
        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
        Response.WriteFile(filePath);
        Response.End();

    }
    protected void show_other(object sender, EventArgs e)
    {

        Response.Redirect("gen_pins.aspx?show=other");


    }
    protected void gen_ecard(object sender, EventArgs e)
    {
        if (txtquantity.Text != "0")
        {
            double quantity = Convert.ToDouble(GetNumberFromStrFaster(txtquantity.Text));
            Int32 epin, ecard;
            string scheme = "x";
            if (!scheme.Equals("p1"))
            {
                double amount = 15;//
                //if (amount == 0) amount = 200;
                amount = amount * quantity;
                if (amount <= ob_userinfo.get_total_income(vars.get_userid()))
                {

                    for (int i = 0; i < quantity; i++)
                    {
                        while (true)
                        {
                            epin = _r.Next(99999);
                            ecard = _r.Next(999999999);
                            if (!ob_userinfo.checkepin(epin, ecard, 0))
                            {

                                //Panel_epin.Visible = true;
                                string epin_str = (DDLamout.SelectedIndex + 1).ToString() + epin.ToString();
                                ob_userinfo.insert_epins_byuser(Convert.ToInt32(epin_str), ecard, scheme, vars.get_userid());
                                ob_userinfo.insert_code_investor_payment(vars.get_userid(), (amount * -1) / quantity);

                                store(epin, ecard);
                                lblmsg.Visible = true;
                                break;

                            }
                        }
                    }
                    new send_email().s_email_sub(ob_userinfo.getemail_addr(vars.get_userid()), "Dear Adverviewer,\n" +
                        "\tThanks For Generating Pins" + "You have generated the following cards:\n\t" + mail_cont +
                        "of scheme " + vars.get_scheme_name(scheme) + "\n" +
                        string.Format("{0:HH:mm:ss tt}", DateTime.Now), "Card generation");
                    Response.Redirect("gen_pins.aspx");

                }
                else
                {
                    lblmsg.Text = "Quantity and amount is above your Earnings";

                }
            }
            else
            {
                lblmsg.Text = "1000 Notes for 1 year is not applicable";

            }
        }
        else
        {
            lblmsg.Text = "Please enter Quantity";
        }

    }

    static string mail_cont = null;
    public void store(Int32 epin, Int32 ecard)
    {
        mail_cont = mail_cont + "Pin 1:" + epin.ToString() + "    Pin 2 :" + ecard.ToString() + "\n";
    }
}