﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class users_mst_user : System.Web.UI.MasterPage
{
    int log = 0;
    cs_vars vars = new cs_vars();
    protected void Page_Load(object sender, EventArgs e)
    {
        string username_c = (string)null;
        string username_s = (string)Session["UserName"];
        HttpCookie cookie = Request.Cookies["UserInfo"];

        if (cookie != null)
        {
            username_c = cookie["UserName"];
            if ((username_c != null) && (username_s == username_c))
            {
                log = 1;
               
            }

        }
        if (log == 0 && Convert.ToInt32(HttpContext.Current.Session["eclicksense"]) == 0)
            Response.Redirect("../login.aspx");
        db_links ob_linkdb = new db_links();
        int lid = ob_linkdb.get_link_id_rev();
        iframe1.Attributes.Add("src", ob_linkdb.get_linkurl_rev(lid));
    }
}
