﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master"AutoEventWireup="true" CodeFile="treeview.aspx.cs" Inherits="treeview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Geneology View || EclickSense ::.</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Tree View of Geneology
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server" />
<asp:UpdatePanel runat="server" id="UpdatePanel1">
    <ContentTemplate>
    
     <asp:updateprogress id="UpdateProgress1"  runat="server" associatedupdatepanelid="UpdatePanel1" dynamiclayout="true">
                        <progresstemplate>
                        <div class="dialog1">

                           <img src="../images/161.gif" class="ajaximg" alt="loading...."/>
                           </div>
                        </progresstemplate>
                    </asp:updateprogress>
                    <div>
                    <div style="float:left">
   <asp:TreeView ID="TreeView1" ShowLines="true" HoverNodeStyle-BackColor="Gray" forecolor="#444444" 
        font-names="Verdana" 
        font-size="0.8em" SelectedNodeStyle-BackColor="Gold"  runat="server" OnSelectedNodeChanged="nodeselect">

             <hovernodestyle cssclass=myclass />
        </asp:TreeView></div>
        <div style="float:right">
         <li><p>Sponsor Id:<asp:Label ID="lblsponsor" runat="server" Text=""></asp:Label></p></li>
                    <li><p>Sponsor Name:<asp:Label ID="lblsponsorname" runat="server" Text=""></asp:Label></p></li>
                    <li><p>Hand: <asp:Label ID="lblsponHand" runat="server" Text=""></asp:Label></p></li>
        </div>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>