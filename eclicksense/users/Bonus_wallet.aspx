﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/users/mst_user.master" CodeFile="Bonus_wallet.aspx.cs" Inherits="users_Bonus_wallet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Bonus Wallet || EclickSense ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Main Wallet
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
 <asp:Panel ID="panel2" CssClass="panel_main" runat="server" GroupingText="Advertisement Views ">
 <br />
 <strong>Per day Advertisement Allowed to View 
    <asp:Label ID="maxclick" runat="server" Text=""></asp:Label><br /><br />
    Today's Viewed Ads: <asp:Label ID="lblcount" runat="server" Text="" ></asp:Label>
<br /><br />
Total click Revenue: <asp:Label ID="lbltorev" runat="server" Text="" ></asp:Label>$
</strong>
</asp:Panel>
<!--<asp:Panel runat ="server" ID="pnlpaging"> Page Number::<asp:DropDownList ID="ddlpage" runat="server" OnSelectedIndexChanged="page_change" AutoPostBack="true">
    </asp:DropDownList></asp:Panel>-->
    <br />
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle"
      EmptyDataText="No Referral or Macthing bonus incomes were recieved."   onrowdatabound="myGridView_RowDataBound"  Font-Size="15px" GridLines="Both" Width="100%" CellPadding="20" CellSpacing="18" >
      <FooterStyle CssClass="GridViewFooterStyle" />
    <RowStyle CssClass="GridViewRowStyle" />   
    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
    <PagerStyle CssClass="GridViewPagerStyle" />
    <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
    <HeaderStyle CssClass="GridViewHeaderStyle" />
    <Columns >

	

		<asp:TemplateField HeaderText="Dollars($)">
		<ItemTemplate>
			<asp:Label id="lblval" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Type">
		<ItemTemplate>
			<asp:Label id="lbltype" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Date ">
		<ItemTemplate>
			<asp:Label id="lbldate" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	

	</Columns>
             </asp:GridView>

     <asp:Panel ID="panel4" CssClass="panel_main" runat="server" GroupingText="Transfer from click to other user">
  <h3>  <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label></h3> <br />
 <table>
    <tr><td>User Name</td><td>
     <asp:TextBox ID="txtuserid" runat="server"></asp:TextBox></td></tr>
      <tr><td>Amount</td><td>
     <asp:TextBox ID="txtamount" runat="server"></asp:TextBox></td></tr>
     <tr><td>Transfer rate now:</td><td>
         <asp:Label ID="lbltrans_rate" runat="server" Text="Transfer season is not active"></asp:Label></td></tr>
          <tr><td>Minimum Transfer amount:</td><td>
         <asp:Label ID="lblmin_amount" runat="server" Text="Transfer season is not active"></asp:Label></td></tr>
         <tr><td colspan="2" align="right">
             <asp:Button ID="Button1" CssClass="button" runat="server" Text="Send Money" OnClick="send_money"  /></td></tr>
 </table>
 
 </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
<asp:Panel ID="pnlconv" CssClass="panel_main" runat="server" GroupingText="Click to Bonus Wallet">

 <h2>  <asp:Label ID="lblmsg2" runat="server" Text=""></asp:Label></h2>
<table>
<tr><td>Amount</td><td><asp:TextBox ID="txtconv" runat="server"></asp:TextBox></td></tr>
<tr>
<td>Minimum convertion amount</td>
<td><asp:Label ID="lblminc_amount" runat="server" Text="Convert season is not active" /></td></tr>
<tr>
<td>Convertion %(how much you can convert)</td>
<td><asp:Label ID="lblmaxc_amount" runat="server" Text="Convert season is not active" /></td></tr>
<tr><td colspan="2" align="right">

<asp:Button ID="Button2" CssClass="button" runat="server" Text="Convert Wallet" OnClick="convert_wallet" /></td></tr>
</table>
</asp:Panel>
<h2>Search by Date</h2>
<asp:ScriptManager ID="ScriptManager1" runat="server" />
    Start Date<asp:TextBox ID="txtsdate" runat="server" Width="150px" Height="25px"></asp:TextBox>  
     <br />
    <asp:CalendarExtender   
        ID="CalendarExtender1"   
        TargetControlID="txtsdate"   
        runat="server" />
        End Date <asp:TextBox ID="txtedate" runat="server" Width="150px" Height="25px"></asp:TextBox>  
        <asp:CalendarExtender   
        ID="CalendarExtender2"   
        TargetControlID="txtedate"   
        runat="server" />
        <br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button CssClass="button" ID="butdate" runat="server" Text="Search" OnClick="date_search" />
</asp:Content>