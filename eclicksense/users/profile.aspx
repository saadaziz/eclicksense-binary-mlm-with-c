﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/users/mst_user.master"CodeFile="profile.aspx.cs" Inherits="profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>Member Profile || EclickSense :: </title>


	
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
    <script src="../js/organictabs.jquery.js"></script>
<style type="text/css">
 /* Generic Utility */
.hide { position: absolute; top: -9999px; left: -9999px; }


/* Specific to example one */


 </style>
<script type="text/javascript">
    $(function () {
        $("#example-one").organicTabs();
    });
		</script>

        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
<br />
My Profile
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<div id="example-one">
        			
        	<ul class="nav">
                <li class="nav-one"><a href="#featured" class="current">Your Profile</a></li>
                <li class="nav-two"><a href="#core">Your Sponsor</a></li>
                <li class="nav-three"><a href="#jquerytuts">Your Last Login</a></li>
                <li class="nav-four last"><a href="#classics">Left & Right Count</a></li>
            </ul>
        	
        	<div class="list-wrap">
        	
        		<ul id="featured">
        			<li><p>User Id: <asp:Label ID="lbl_infoid" runat="server" Text=""></asp:Label> </p></li>
        			<li><p>Full Name:<asp:Label ID="lbl_infofull" runat="server" Text=""></asp:Label></p></li>
        			<li><p>Address: <asp:Label ID="lbl_addr" runat="server" Text=""></asp:Label></p></li>
        			<li><p>Email: <asp:Label ID="lbl_email" runat="server" Text=""></asp:Label></p></li>
        			<li><p>Contact: <asp:Label ID="lbl_contact" runat="server" Text=""></asp:Label></p></li>
        			<li><p>Scheme: <asp:Label ID="lbl_infoscheme" runat="server" Text=""></asp:Label></p></li>
        			<li><p>Joining Date: <asp:Label ID="lbl_doj" runat="server" Text=""></asp:Label></p></li>
        		</ul>
        		 
        		 <ul id="core" class="hide">
                    <li><p>Sponsor Id:<asp:Label ID="lblsponsor" runat="server" Text=""></asp:Label></p></li>
                    <li><p>Sponsor Name:<asp:Label ID="lblsponsorname" runat="server" Text=""></asp:Label></p></li>
                    <li><p>Hand: <asp:Label ID="lblsponHand" runat="server" Text=""></asp:Label></p></li>
                  
        		 </ul>
        		 
        		 <ul id="jquerytuts" class="hide">
        		  <li> <p> Last Login: <asp:Label ID="lbl_infologin" runat="server" Text=""></asp:Label></p></li>
        		   </ul>
        		 
        		 <ul id="classics" class="hide">
                  <li> <p> Left Team Has: <asp:Label ID="lblhand" runat="server" Text=""></asp:Label></p></li>
                   <li> <p> Right Team Has: <asp:Label ID="lblrhand" runat="server" Text=""></asp:Label></p></li>
                   </ul>
        		 
        	 </div> <!-- END List Wrap -->
         
         </div> 

			

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
   
</asp:Content>