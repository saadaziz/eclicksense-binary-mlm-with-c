﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class users_geneology : System.Web.UI.Page
{
    int log = 0;
    db_control ob_user = new db_control();
    cs_vars vars = new cs_vars();
    int[] childs = new int[15];
    gen_db ob_userinfo = new gen_db();
    db_par get_ch = new db_par();
    get_child ob_gen = new get_child();
    static List<Int32> child_lstl = new List<Int32>();

    static List<Int32> child_lstr = new List<Int32>();
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hplgen");
        hpl.CssClass = "active";
        int root = 0;
        if (!IsPostBack)
        {

            root = vars.get_userid();
            //child_lstl = get_ch.get_lchild_list(vars.get_userid());
            //child_lstr = (ob_userinfo.getRcount(vars.get_userid()).ToString();
            lblright.Text = ob_userinfo.getRcount(vars.get_userid()).ToString();
            lblleft.Text = ob_userinfo.getLcount(vars.get_userid()).ToString();
        }
        int root1 = 0, i = 0, root2 = 0, rootgc3 = 0, rootgc4 = 0, rootgc5 = 0, rootgc6 = 0;
        //int rootgc7 = 0, rootgc8 = 0, rootgc9 = 0, rootgc10 = 0, rootgc11 = 0, rootgc12 = 0, rootgc13 = 0, rootgc14 = 0;
        Int32 direct_l = 0, direct_r = 0;

        string userid = ob_user.get_username(vars.get_userid());
        lbllall.Text = (ob_userinfo.get_left_money_all(vars.get_userid())).ToString();
        lblrall.Text = (ob_userinfo.get_right_money_all(vars.get_userid())).ToString();
        lbllmony.Text = (ob_userinfo.get_left_money(vars.get_userid())).ToString();
        lbluser.Text = userid;
        lblrmony.Text = (ob_userinfo.get_right_money(vars.get_userid())).ToString();

        
        userid = Convert.ToString(Request.QueryString["userid"]);

        if ((userid != "")/* && (ob_userinfo.checkchild(vars.get_userid(), Convert.ToInt32(GetNumberFromStrFaster(userid))))*/)
        {
            //Session.Add("root", Convert.ToString(Convert.ToInt32( GetNumberFromStrFaster(userid))));
            root = Convert.ToInt32(ob_user.get_userid_uname( userid));
        }
        else
            root = vars.get_root();

        if (root == 0)
            root = vars.get_userid();
        if (root == vars.get_userid())
            but_up.Visible = false;
        List<Int32> gen_list = ob_gen.get_child_list_gen(root);//get children
        //gen_list.Remove(userid);
        lblroot.Text = ob_user.get_username(root); 

        int rc = gen_list[2];
        if (rc != 0)
        {
            lblc2.Text = ob_user.get_username(rc); 
            imgc2.ImageUrl = "images/green.png";
            root2 = rc;
            childs[i] = rc; i++;
            direct_r = rc;
            List<string> data = ob_userinfo.get_Userinfo_mod(rc);
            lblpopuname2.Text = data[0];
            lblpopfname2.Text = data[1];
            lblpopaddr2.Text = data[2];
            lblpopdoj2.Text = data[3];
            lblpoplc2.Text = data[4];
            lblpoprc2.Text = data[5];
            // imgc2.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(rc).ToString());
        }
        int lc = gen_list[1];
        if (lc != 0)
        {
            root1 = lc;
            childs[i] = lc; i++;
            lblc1.Text = ob_user.get_username(lc);
            imgc1.ImageUrl = "images/green.png";
            direct_l = lc;
            List<string> data = ob_userinfo.get_Userinfo_mod(lc);
            lblpopuname.Text = data[0];
            lblpopfname.Text = data[1];
            lblpopaddr.Text = data[2];
            lblpopDOJ.Text = data[3];
            lblpoplc.Text = data[4];
            lblpoprc.Text = data[5];
            // imgc1.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(lc).ToString());
        }

        //3rd layer
        lc = gen_list[3];
        if (lc != 0)
        {
            List<string> data = ob_userinfo.get_Userinfo_mod(lc);
            lblpopuname3.Text = data[0];
            lblpopfname3.Text = data[1];
            lblpopaddr3.Text = data[2];
            lblpopdoj3.Text = data[3];
            lblpoplc3.Text = data[4];
            lblpoprc3.Text = data[5];

            lblgc3.Text = ob_user.get_username(lc);
            imggc3.ImageUrl = "images/green.png";
            rootgc3 = lc; childs[i] = lc; i++;
            //   imggc3.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(lc).ToString());
        }
        rc = gen_list[4];
        if (rc != 0)
        {
            List<string> data = ob_userinfo.get_Userinfo_mod(rc);
            lblpopuname4.Text = data[0];
            lblpopfname4.Text = data[1];
            lblpopaddr4.Text = data[2];
            lblpopdoj4.Text = data[3];
            lblpoplc4.Text = data[4];
            lblpoprc4.Text = data[5];

            lblgc4.Text = ob_user.get_username(rc); 
            imggc4.ImageUrl = "images/green.png";
            rootgc4 = rc; childs[i] = rc; i++;
            // imggc4.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(rc).ToString());
        }
        lc = gen_list[5];
        if (lc != 0)
        {
            List<string> data = ob_userinfo.get_Userinfo_mod(lc);
            lblpopuname5.Text = data[0];
            lblpopfname5.Text = data[1];
            lblpopaddr5.Text = data[2];
            lblpopdoj5.Text = data[3];
            lblpoplc5.Text = data[4];
            lblpoprc5.Text = data[5];

            lblgc5.Text = ob_user.get_username(lc);
            imggc5.ImageUrl = "images/green.png";
            rootgc5 = lc; childs[i] = lc; i++;
            // imggc5.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(lc).ToString());
        }
        rc = gen_list[6];
        if (rc != 0)
        {
            List<string> data = ob_userinfo.get_Userinfo_mod(rc);
            lblpopuname6.Text = data[0];
            lblpopfname6.Text = data[1];
            lblpopaddr6.Text = data[2];
            lblpopdoj6.Text = data[3];
            lblpoplc6.Text = data[4];
            lblpoprc6.Text = data[5];

            lblgc6.Text = ob_user.get_username(rc); 
            imggc6.ImageUrl = "images/green.png";
            rootgc6 = rc; childs[i] = rc; i++;
            // imggc6.ToolTip = vars.get_scheme_name(ob_userinfo.get_scheme(rc).ToString());
        }

        //4th layer


        //sidebar
        string[] data1 = new string[8];
        data1 = ob_userinfo.get_Userinfo(vars.get_root());
        lbl_infoid.Text = ob_user.get_username(root); 
        lbl_infofull.Text = data1[0];
        lbl_doj.Text = data1[1];
        //lbl_inforef.Text = ob_user.get_username(Convert.ToInt32( data[2]));


    }
    protected void get_up(object sender, EventArgs e)
    {

        Session.Add("root", Convert.ToString(vars.get_userid()));

        Response.Redirect("geneology.aspx");
        /*Int16 gen_up = 0;
        for (int i = 0; i < 3; i++)
        {
            //Response.Write("Hi there");
            gen_up = ob_userinfo.get_parent(vars.get_root());

            if (gen_up == vars.get_userid())
            {
                Session.Add("root", Convert.ToString(gen_up));
                break;
            }
            else
                Session.Add("root", Convert.ToString(gen_up));


        }*/
        //Session.Add("root", Convert.ToString(gen_up));
        //Session.Add("arveruser", Convert.ToString(gen_up));

        //Response.Redirect("geneology.aspx");

    }
    protected string GetNumberFromStrFaster(string str)
    {
        if (str != null)
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
        else
            return "0";
    }
    /* protected bool avail_children(Int32 id)
     {
         if (child_lstl.Contains(id)) return true;
         else if (child_lstr.Contains(id)) return true;
         else return false;
     }
     public void tmp_func()
     {

         Int32 tmp = Convert.ToInt32(GetNumberFromStrFaster(txtsearch.Text).ToString());
         if (avail_children(tmp))
         {

             Session.Add("root", Convert.ToString(tmp));
             but_up.Visible = true;
             Response.Redirect("geneology.aspx");
         }
         else
             lblid.Text = "This user is not in your downline";

     }*/
    protected void set_sidebar(object sender, CommandEventArgs e)
    {
        Int16 inp = Convert.ToInt16(e.CommandArgument.ToString());
        if (inp == 3)
            txtsearch.Text = lblgc3.Text;
        else if (inp == 4)
            txtsearch.Text = lblgc4.Text;
        else if (inp == 5)
            txtsearch.Text = lblgc5.Text;
        else if (inp == 6)
            txtsearch.Text = lblgc6.Text;
        else if (inp == 1)
            txtsearch.Text = lblc1.Text;
        else if (inp == 2)
            txtsearch.Text = lblc2.Text;
        else
        {
            txtsearch.Text = "0";
            Response.Redirect("geneology.aspx");
        }


        //tmp_func();
        //previous codes
        if (txtsearch.Text != null)
            Response.Redirect("geneology.aspx?userid=" + txtsearch.Text);
        else
            Response.Redirect("profile.aspx");
        //int tmp = Convert.ToInt32(GetNumberFromStrFaster(lbl.Text));
        //vars.set_root(tmp);

    }
}