﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master"AutoEventWireup="true" CodeFile="notification.aspx.cs" Inherits="notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title> ..:: My Notifications|| ClickSourcer ::..</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
My Notifications
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
        <FooterTemplate>
           <h2><asp:Label ID="lblEmptyData"
            Text="No Notifications To Display" runat="server" Visible="false">
            </asp:Label></h2>
     </FooterTemplate>

         <ItemTemplate>
        <div class="box post">
        <div class="content">
          <div class="post-title">
            <h2><a><asp:Label ID="lbsub" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "subject") %>' ></asp:Label> </a></h2>
          </div>
          <div class="post-date">On&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbldate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "datee") %>' ></asp:Label> 
          </div>
          <br /><p>Type: &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "type") %>' ></asp:Label></p>
          <div class="post-excerpt">
            <p><asp:Label ID="lblnotice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Notice") %>' ></asp:Label> </p>
          </div>
          <div class="clr"></div>
        </div>
      </div>
            </ItemTemplate>
</asp:Repeater>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>