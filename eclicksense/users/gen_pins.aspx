﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master" AutoEventWireup="true" CodeFile="gen_pins.aspx.cs" Inherits="gen_pins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Generate Pins|| EclickSense.com ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Generate New Pins
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<h2><asp:Label ID="lblmsg" runat="server" Text=""></asp:Label></h2>
  <table class="table_class">
 
    
    <tr><td>Your Membership Wallet contains </td>
    <td>
        <asp:Label ID="lblbalance" runat="server" Text=""></asp:Label></td></tr>
    <tr>
    <td>
        Enter Amount</td><td>
        <asp:DropDownList ID="DDLamout" runat="server" Width="150px">
            <asp:ListItem>15$</asp:ListItem>
        </asp:DropDownList></td></tr>

        <tr>
            <td>
                Quantity</td><td><asp:TextBox ID="txtquantity" runat="server" Text="1" Width="150px"></asp:TextBox></td>
        
        </tr>
        <tr><td>
            Check to see only usable pins<asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="show_other" AutoPostBack="true" /></td><td >
            <asp:Button ID="but_gen" runat="server" CssClass="button" Text="Generate Pins" OnClick="gen_ecard"  /></td></tr>
        </table>
        
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle"
            onrowdatabound="myGridView_RowDataBound" Font-Size="Small" GridLines="Both" HorizontalAlign="Left" Width="100%" CellPadding="5" CellSpacing="8" >
            <FooterStyle CssClass="GridViewFooterStyle" />
    <RowStyle CssClass="GridViewRowStyle" />   
    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
    <PagerStyle CssClass="GridViewPagerStyle" />
    <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
    <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns >
            <asp:TemplateField HeaderText="Pin ID" >
		<ItemTemplate>
			<asp:Label id="lbleid" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

 	<asp:TemplateField HeaderText="Pin A">
		<ItemTemplate>
			<asp:Label id="lblepin" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

		<asp:TemplateField HeaderText="Pin B">
		<ItemTemplate>
			<asp:Label id="lblecode" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>

    <asp:TemplateField HeaderText="Assigned User">
		<ItemTemplate>
			<asp:Label id="lbluser" runat="server"></asp:Label>
		</ItemTemplate>
	</asp:TemplateField>	

	</Columns>
    
            </asp:GridView>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">
</asp:Content>