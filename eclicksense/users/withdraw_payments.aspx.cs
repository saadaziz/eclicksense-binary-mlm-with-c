﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class users_withdraw_payments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cs_vars vars = new cs_vars();
        lblclickwallet.Text = (new db_links().get_sum_amount(vars.get_userid()) * .1).ToString();
        lblbonuswallet.Text = (new db_payment().sum_payment(vars.get_userid())).ToString();
    }
}