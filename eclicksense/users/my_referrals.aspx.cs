﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class my_referrals : System.Web.UI.Page
{
    //int log = 0;
    cs_vars vars = new cs_vars();
    gen_db ob_db = new gen_db();
    List<Int32> Lchild_list = new List<Int32>();
    List<Int32> Rchild_list = new List<Int32>();
    protected void Page_Load(object sender, EventArgs e)
    {

        System.Web.UI.HtmlControls.HtmlGenericControl home = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("ulall").FindControl("lirefers");

        home.Attributes["class"] = "active";
        Lchild_list = ob_db.get_spon_list(vars.get_userid(),"L");
        foreach (int child in Lchild_list)
        {
            lstluserlist.Items.Add("eclick" + child.ToString());
        }
        lblluser.Text = lstluserlist.Items.Count.ToString();

        Rchild_list = ob_db.get_spon_list(vars.get_userid(), "R");
        foreach (int child in Rchild_list)
        {
            lstruserlist.Items.Add("eclick" + child.ToString());
        }
        lblruser.Text = lstruserlist.Items.Count.ToString();
        
    }
}