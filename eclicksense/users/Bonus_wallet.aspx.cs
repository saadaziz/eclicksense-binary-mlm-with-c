﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class users_Bonus_wallet : System.Web.UI.Page
{
    db_payment ob_dbpay = new db_payment();
    cs_vars vars = new cs_vars();

    db_control ob_link = new db_control();
    db_links ob_linkdb = new db_links();
    protected void date_search(object sender, EventArgs e)
    {
        /* string[] dateofbirth = dob.Split('/');
         dob = dateofbirth[2] + "-" + dateofbirth[0] + "-" + dateofbirth[1];*/
        Response.Redirect("bonus_wallet.aspx?sdate=" + txtsdate.Text.Split('/')[2] + "-" + txtsdate.Text.Split('/')[0] + "-" + txtsdate.Text.Split('/')[1] + "&edate=" + txtedate.Text.Split('/')[2] + "-" + txtedate.Text.Split('/')[0] + "-" + txtedate.Text.Split('/')[1]);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hplbonus");
        hpl.CssClass = "active";
        Int32 countt = ob_linkdb.get_count_2day(vars.get_userid());
        lblcount.Text = "   " + Convert.ToString(countt);
        maxclick.Text = ob_link.get_var("links_page").ToString();

        lbltorev.Text = (ob_linkdb.get_sum_amount(vars.get_userid()) * .1).ToString();
        string edate = null;
        edate = Convert.ToString(Request.QueryString["edate"]);
        string sdate = null;
        sdate = Convert.ToString(Request.QueryString["sdate"]);

        Int32 pageid = Convert.ToInt32(Request.QueryString["pageid"]);
        Int32 totpage = ob_dbpay.get_count_page(vars.get_userid());
        if (totpage > 1)
        {
            for (int i = 1; i <= totpage; i++)
                ddlpage.Items.Add(i.ToString());
        }
        else
            pnlpaging.Visible = false;
        string strSQL="";
        GridView1.DataSource = null;
        GridView1.DataBind();

        if (pageid != 0)
            strSQL = "SELECT sum(notes) as amount,type,date_format(db_payment.time,'%Y-%m-%d ') as dd from db_payment where uid = " + vars.get_userid() + " group by type limit " + (10 * pageid - 10).ToString() + "," + 10 * pageid + ";";
        else if (sdate != null)
        {
            strSQL = "SELECT sum(notes) as amount,type,date_format(db_payment.time,'%Y-%m-%d ') as dd from db_payment where uid = " + vars.get_userid() + " and (time<=date_format('" + edate + "','%Y-%c-%d')  and time>=date_format('" + sdate + "','%Y-%c-%d') ) group by type order by dd desc limit 0,150;";
            //Response.Write(strSQL);
        }
        else
            strSQL = "SELECT sum(notes) as amount,type,date_format(db_payment.time,'%Y-%m-%d ') as dd from db_payment where uid = " + vars.get_userid() + "  group by type limit 0,15;";
        
        new binder().BindData(strSQL, GridView1);

        if (ob_link.get_var("transfer period") == 0)
            Button1.Enabled = false;
        else
        {
            double trans_rate = ob_link.get_var("maximum transfer percentage");
            double trans_amount = ob_link.get_var("minimum transfer amount");
            lbltrans_rate.Text = (trans_rate * 100) + "%";
            lblmin_amount.Text = trans_amount + "$";
        }
        if (ob_link.get_var("switch wallet") != 0)
        {
            double conv_rate = ob_link.get_var("maximum convert percentage");
            double min_conv = ob_link.get_var("minimum convert amount");
            lblmaxc_amount.Text = conv_rate * 100 + "%";
            lblminc_amount.Text = min_conv + "$";
        }
        else
            Button2.Enabled = false;
    }
    public string GetNumberFromStrFaster(string str)
    {
        if (str != null)
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));

        else
            return "0";
    }
    protected void send_money(object sender, EventArgs e)
    {

        if (ob_link.get_var("transfer period") != 0)
        {
            double trans_rate = ob_link.get_var("maximum transfer percentage");
            double trans_amount = ob_link.get_var("minimum transfer amount");
            double user_amount = Convert.ToDouble(vars.GetNumberFromStrFaster(txtamount.Text));
            double curr_balance = ob_linkdb.get_sum_amount(vars.get_userid()) * .1;

            Int32 send_id = Convert.ToInt32(ob_link.get_userid_uname((txtuserid.Text).Trim()));

            if (send_id != 0)
            {
                if (user_amount <= (curr_balance * trans_rate))
                {
                    if (user_amount >= trans_amount)
                    {
                        double click_count = user_amount / .1;
                        ob_linkdb.send_click_amount(vars.get_userid(), send_id, click_count);
                        lbltorev.Text = (ob_linkdb.get_sum_amount(vars.get_userid()) * .1).ToString();

                        lblmsg.Text = "";
                        Response.Redirect("Bonus_wallet.aspx");
                    }
                    else
                        lblmsg.Text = "Cannot send amount lower than minimum amount: " + trans_amount.ToString();

                }
                else
                    lblmsg.Text = "Your Amount is greater than transfer amount( Current Balance*Transfer Rate): " + curr_balance * trans_rate + " .";
            }
            else
                lblmsg.Text = "Click Wallet Transfer period is off.";
        }
        else
            lblmsg.Text = "Userid not found, Give a valid User ID.";
    }
    protected void convert_wallet(object sender, EventArgs e)
    {

        if (ob_link.get_var("switch wallet") != 0)
        {
            double conv_rate = ob_link.get_var("maximum convert percentage");
            double min_conv = ob_link.get_var("minimum convert amount");
            double user_amount = Convert.ToDouble(vars.GetNumberFromStrFaster(txtconv.Text));
            double curr_balance = (ob_linkdb.get_sum_amount(vars.get_userid()) * .1);
            if (user_amount < (curr_balance * conv_rate))
            {
                if (user_amount >= min_conv)
                {
                    double click_count = user_amount / .1;

                    ob_linkdb.send_click_amount(vars.get_userid(), click_count);
                    new db_payment().convert_click(vars.get_userid(), user_amount);
                    lbltorev.Text = (ob_linkdb.get_sum_amount(vars.get_userid()) * .1).ToString();
                    lblmsg2.Text = "";
                    Response.Redirect("Bonus_wallet.aspx");
                }
                else
                    lblmsg2.Text = "Cannot convert amount lower than minimum amount: " + lblminc_amount.Text;

            }
            else
                lblmsg2.Text = "Your Amount is greater than transfer amount( Current Balance*Transfer Rate ): " + curr_balance * conv_rate + " .";
        }
        else
            lblmsg2.Text = "Wallet conversion period is off.";
    }
      
    protected void page_change(object sender, EventArgs e)
    {
        Response.Redirect("bonus_wallet.aspx?pageid=" + ddlpage.SelectedValue.ToString());
    }
    protected void myGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        Label lbldate = (Label)e.Row.FindControl("lbldate");
        if ((lbldate != null))
        {

            lbldate.Text = DataBinder.Eval(e.Row.DataItem, "dd").ToString();
            
        }

        
        Label lblval = (Label)e.Row.FindControl("lblval");
        if ((lblval != null))
        {
            lblval.Text = DataBinder.Eval(e.Row.DataItem, "amount").ToString() +"$";

        }
        Label lbltype = (Label)e.Row.FindControl("lbltype");
        if ((lbltype != null))
        {
            lbltype.Text = DataBinder.Eval(e.Row.DataItem, "type").ToString();

        }

    }
}