﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Web.UI.HtmlControls;
public partial class profile : System.Web.UI.Page
{
    int log = 0;
    cs_vars vars = new cs_vars();
    db_control ob_userinfo = new db_control();
    pdf make_pdf = new pdf();

    protected void Page_Load(object sender, EventArgs e)
    {

        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hplpro");
        hpl.CssClass = "active";
        
        vars.set_rand(null);
       
        /*List<string> usercontact = ob_userinfo.get_contactinfo(vars.get_userid());
        imggmail.ToolTip = usercontact[0];
        imgyahoo.ToolTip = usercontact[1];
        imgskype.ToolTip = usercontact[2];
        imgface.ToolTip = usercontact[3];*/
        //vars.set_root(0);
        int userid = vars.get_userid();
        string[] data = new string[8];
        data = ob_userinfo.get_Userinfo(userid);
        lbl_infoid.Text = ob_userinfo.get_username(userid);
        lbl_infofull.Text = data[0];
        lbl_addr.Text = data[1];
        lbl_contact.Text = data[3];
        lbl_email.Text = data[2];
        lbl_infoscheme.Text = vars.get_scheme_name(data[4]);
        lbl_doj.Text = data[5];
        lbl_infologin.Text = data[7];

        lblhand.Text = ob_userinfo.getLcount(vars.get_userid()).ToString();
        lblrhand.Text = ob_userinfo.getRcount(vars.get_userid()).ToString();
        Int32 sponid = (ob_userinfo.get_sponid(userid));
        if (sponid == -1)
            lblsponsor.Text = "Root";
        else
        {
            lblsponsor.Text = "userid" + sponid.ToString();



            lblsponsorname.Text = (ob_userinfo.get_uname(Convert.ToInt32(sponid))).ToString();

            lblsponHand.Text = ob_userinfo.get_hand(vars.get_userid(), "db_sponsors");
        }

        
    }
   
}