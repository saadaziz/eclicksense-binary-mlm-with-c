/*
 * jQuery Easing v1.3
 * http://gsgd.co.uk/sandbox/jquery/easing/
 * http://jquery.com/
*/

jQuery.noConflict(); jQuery(document).ready(function(){

var autocarousel = 1;
var autocarousel_delay = 1800; // delay before next slide

if(autocarousel_delay == '' || autocarousel_delay == undefined) autocarousel_delay = '7000'; if(autocarousel != 1) autocarousel_delay = false; if (jQuery('.featured_item').length > 0 )jQuery('#featured').carousel({	duration:600,	transition:"easeInOutCubic", opacity:0.6,	opacity_level2:0,	interval: autocarousel_delay	}); }); (function($) { 	$.fn.carousel = function(default_options)	{	var defaults =	{	duration:600,	transition:"easeInOutCubic", opacity:0.6, opacity_level2:0, interval: 5000 };	var def = $.extend(defaults, default_options);	return this.each(function()	{	var $container = $(this), $items = $container.find('.featured_item'), $values = [], $zindex = [], $offset = 0, $animating = false, $clicked=false; var interval = setInterval(function(){}, 60000); jQuery(window).load(function(){ if(def.interval && !$clicked )	{	interval = setInterval(function() { rotate(-1); }, def.interval);	}	});	if (def.opacity != 1 && def.opacity_level2 != 1) {	$items.not('.featured_item_active').css('opacity',def.opacity_level2); $container.find('.featured_item_active').css('opacity', 1);	$container.find('.featured_item_last, .featured_item_upcoming').css('opacity', def.opacity);	}	$items.each(function(i)	{	var $item = $(this); $values[i]= { width: $item.width(), top: parseInt($item.css('top')), left: parseInt($item.css('left')), opacity: $item.css('opacity') }; $zindex[i] =	$item.css('zIndex');	});	$items.click(function(e)	{	if (! $animating)	{	$direction = e.pageX > $(window).width() / 2 ? -1 : 1;	rotate($direction);	}	clearInterval(interval);$clicked = true;});	function rotate($direction)	{	if ($items.length <= 2) return;	$animating = true; if($items.length == $offset || $items.length == ($offset*-1)) {$offset = 1 * $direction;	}	else	{	$offset = $offset + $direction;	}	$items.each(function(i)	{	var $item = $(this), $next;	$next = i + $offset; if($next >= $items.length) {	$next = i - $items.length + $offset; } else if($next < 0) { $next = i + $items.length + $offset;}	$item.animate($values[$next], def.duration, def.transition); $item.find("img").animate({width:$values[$next].width}, def.duration, def.transition, function() { $animating = false;	});	setTimeout(function()	{ $item.css({zIndex: $zindex[$next]}); }, def.duration / 2);});	}	});	};})(jQuery);	jQuery.easing['jswing'] = jQuery.easing['swing']; jQuery.extend( jQuery.easing, { 	def: 'easeOutQuad',	swing: function (x, t, b, c, d) {		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);	},	easeInQuad: function (x, t, b, c, d) {		return c*(t/=d)*t + b;	},	easeOutQuad: function (x, t, b, c, d) {		return -c *(t/=d)*(t-2) + b;	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});
