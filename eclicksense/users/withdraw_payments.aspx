﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/users/mst_user.master" CodeFile="withdraw_payments.aspx.cs" Inherits="users_withdraw_payments" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: Withdraw Income || EclickSense ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Withdraw Income from Wallets
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">

    <h2>Ypu do not have sufficient amount in wallets to withdraw.</h2>
    <table>
    <tr><td>Your Click Wallet:</td><td>
        <asp:Label ID="lblclickwallet" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Your Bonus Wallet:</td><td>
        <asp:Label ID="lblbonuswallet" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Enter Amount to withdraw</td><td>
        <asp:TextBox ID="txtamount" runat="server"></asp:TextBox></td></tr></table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>