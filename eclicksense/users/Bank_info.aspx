﻿<%@ Page Language="C#" MasterPageFile="~/users/mst_user.master" AutoEventWireup="true" CodeFile="Bank_info.aspx.cs" Inherits="users_Bank_info" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" Runat="Server">
<title>.:: banking Information || EclickSense ::.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphheader" Runat="Server">
Banking Information
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphbody" Runat="Server">
<h2>
    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label></h2>
   <table class="table_class">
                <tr><td>Account Holder Name(As per bank record): </td><td>
                    <asp:TextBox ID="txthname" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                        <tr><td>Account Name: </td><td>
                    <asp:TextBox ID="txtaname" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                        <tr><td>Account Type: </td><td>
                    <asp:TextBox ID="txtatype" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                        <tr><td>Bank Branch Name: </td><td>
                    <asp:TextBox ID="txtbcode" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                                <tr><td>Bank Branch Name: </td><td>
                    <asp:TextBox ID="TextBox3" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                                <tr><td>IFSC Code: </td><td>
                    <asp:TextBox ID="TextBox4" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                                <tr><td>Swift Code: </td><td>
                    <asp:TextBox ID="TextBox5" MaxLength="100" runat="server"></asp:TextBox></td></tr>
                                <tr><td>TIN Card No.: </td><td>
                    <asp:TextBox ID="TextBox6" MaxLength="100" runat="server"></asp:TextBox></td></tr>
            <tr><td colspan="2" align="center">
                <asp:Button ID="butbsub" CssClass="button" runat="server" Text="Submit" OnClick="sub_bank" /></td></tr>    
        </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphright" Runat="Server">

</asp:Content>