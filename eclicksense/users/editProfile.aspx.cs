﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class users_editProfile : System.Web.UI.Page
{
    db_control ob_db = new db_control();
    cs_vars vars = new cs_vars();
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hpledit");
        hpl.CssClass = "active";

        int userid = vars.get_userid();
        string[] data = new string[8];
        data = ob_db.get_Userinfo(userid);
        lbl_infoid.Text = ob_db.get_username(userid);
        lbl_infofull.Text = data[0];
        lbl_addr.Text = data[1];
        lbl_contact.Text = data[3];
        lbl_email.Text = data[2];


    }
    /* protected void sub_tran(object sender, EventArgs e)
     {
         if ((txttransaction.Text.Length > 3))
         {
            
             ob_db.insert_trans(Convert.ToInt32(GetNumberFromStrFaster(txttransaction.Text)), vars.get_userid());
             new send_email().s_email_sub(ob_db.getemail_addr(vars.get_userid()), "Transaction Pin is: " + txttransaction.Text, "Transaction pin changed");
         }
     }*/
    protected string GetNumberFromStrFaster(string str)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
    }
    protected void change_pass(object sender, EventArgs e)
    {
        string email = ob_db.getemail_addr(vars.get_userid());
        Int32 user = vars.get_userid();
        if (txtnewpass.Text.ToString().Equals(txtrenew.Text.ToString()))
        {
            if ((txtnewpass.Text).ToString().Length >= 6)
            {
                if (ob_db.get_pass(user) == txtold.Text)
                {
                    ob_db.update_user_pass(vars.get_userid(), txtnewpass.Text);
                    lblmsg.Text = "Password/Contact Successfully changed";
                    string cmd = "Dear Sir,\n\tWe are informing as a priviledged member your userid is: eclick" +
                                      vars.get_userid() +
                                     "and password(which you has been changed to): " +
                                     txtnewpass.Text +
                                     "\n";

                    if (email != "")
                    {
                        send_email email_ob = new send_email();
                        email_ob.s_email(email, cmd);

                    }
                }
                else if (ob_db.get_pass(user) != txtold.Text)
                    lblmsg.Text = "Your old password didn't match.";
            }
            else if ((txtnewpass.Text).ToString().Length < 5)
                lblmsg.Text = "Please enter 6 characters for password";
        }
        else
            lblmsg.Text = "Your new passwords didn't match";

    }
    protected void sub_bank(object sender, EventArgs e)
    {
        lblmsg.Text = "Banking data saved";
    }
    protected void change_email(object sender, EventArgs e)
    {
        if (txtemail.Text != "" && isEmail(txtemail.Text))
        {
            ob_db.update_user_email(Convert.ToInt32(vars.get_userid()), txtemail.Text);
            Response.Redirect("editProfile.aspx");
        }
        else
            lblmsg.Text = "Text Field Empty or Email not valid";
    }
    public static bool isEmail(string inputEmail)
    {
        //inputEmail = NulltoString(inputEmail);
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return (true);
        else
            return (false);
    }
    protected void change_full(object sender, EventArgs e)
    {
        if (txtnewname.Text != null)
        {
            ob_db.update_fullName(txtnewname.Text, vars.get_userid());
            Response.Redirect("editProfile.aspx");
        }
        else
            lblmsg.Text = "Please Insert Name in the Text";
    }
    /*protected void insert_contact(object sender, EventArgs e)
    {
        if ((txtgmail.Text != null)&&(txtfacebook.Text!=null)&&(txtskype.Text!=null)&&(txtyahoo.Text!=null))
        {
            ob_db.insert_contacts(vars.get_userid(), txtgmail.Text, txtfacebook.Text, txtyahoo.Text,txtskype.Text);
            //Response.Redirect("edit_account.aspx");
            lblmsg.Text = "All Contact Information inserted";
        }
        else
            lblmsg.Text = "Please Insert All the fields";
    }*/
    protected void change_contact(object sender, EventArgs e)
    {
        if (txtnewcontact.Text != null)
        {
            ob_db.update_contact(txtnewcontact.Text, vars.get_userid());
            Response.Redirect("editProfile.aspx");
        }
        else
            lblmsg.Text = "Please Insert Contact in the Text";
    }
    /* protected void change_address(object sender, EventArgs e)
     {
         if (txtnewaddress.Text != null)
         {
             ob_db.update_address(txtnewaddress.Text, vars.get_userid());
             Response.Redirect("edit_account.aspx");
         }
         else
             lblmsg.Text = "Please Insert Address in the Text";
     }*/
}