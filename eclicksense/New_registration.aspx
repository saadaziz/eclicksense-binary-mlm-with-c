﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="New_registration.aspx.cs" Inherits="New_registration" Title="New Member Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <link type="text/css" href="jquery.ibutton.css" rel="stylesheet" media="all" />
	
	<link rel="stylesheet" type="text/css" href="reg_css/style.css" media="screen" />
	

	<script src="reg_js/jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
	<script src="reg_js/form-fun.jquery.js" type="text/javascript" charset="utf-8"></script>
          <script type="text/javascript" src="js/jquery.ibutton.js"></script>
  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.metadata.js"></script>
	<script type="text/javascript">
	    $(document).ready(function () {
	    $("#num_attendees").iButton();
	    });
</script>
<script type="text/javascript">
    function showPopUp1(el) {
        var cvr = document.getElementById("cover1")
        var dlg = document.getElementById(el)
        cvr.style.display = "block"
        dlg.style.display = "block"
        if (document.body.style.overflow = "hidden") {
            cvr.style.width = "1024"
            cvr.style.height = "100%"
        }
    }
    function closePopUp1(el) {
        var cvr = document.getElementById("cover1")
        var dlg = document.getElementById(el)
        cvr.style.display = "none"
        dlg.style.display = "none"
        document.body.style.overflowY = "scroll"
    }
</script>
	<!--[if IE]>
		<style type="text/css">
			legend { 
				position: relative;
				top: -30px;
			}
			
			fieldset {
				margin: 30px 10px 0 0;
			}
		</style>
		
		<script type="text/javascript">
			$(function(){
				$("#step_2 legend").css({ opacity: 0.5 });
				$("#step_3 legend").css({ opacity: 0.5 });
			});
		</script>
	<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="page-wrap">
		
		<h1>Member <span>Registration</span></h1>
        
        <h1><span>
<asp:Label ID="lblmsg" runat="server"></asp:Label></span></h1>
         <div id="cover1"></div>
<div id="dialog1"><br />
   
        <a class="close" href="login.aspx"></a>
        <br />
<asp:TextBox Enabled="false" ID="lblsuccess" runat="server" Text="" TextMode="MultiLine" Width="98%" Height="100%"></asp:TextBox><br /><br />


</div>
		

			<fieldset id="step_1">
			
<br />
				<legend>Select Sponsor</legend>
			
				<label for="num_attendees1">
					Which Hand?
				</label>
                <input type="checkbox" id="num_attendees" runat="server" class="{labelOn: 'Left', labelOff: 'Right', easing: 'easeOutBounce', duration: 500}" />
				
			
				<br />
			
				<div id="attendee_1_wrap" class="name_wrap push">
					<h3>Please provide Sponsor Id:</h3>
				
					<label for="name_attendee_1">
							Sponsor (Login)UserName:
					</label>
        <asp:TextBox id="name_attendee_1"  CssClass="name_input" runat="server"></asp:TextBox>
				</div>
			
			</fieldset>
		
			<fieldset id="step_2">
			
				<legend>User Information</legend>
			<p>Click all the texts and Fill them for next Step</p>
				<p>
					Login Username
				</p>
				 <asp:TextBox id="txtusername" MaxLength="20" CssClass="gunenrtext"  runat="server"></asp:TextBox>
		    <br />
            <p>
			    Password
				</p>
				 <asp:TextBox id="txtpassword" MaxLength="20" TextMode="Password"  CssClass="gunenrtext" runat="server"></asp:TextBox>
				<br />
                <p>
					Email Address
				</p>
				 <asp:TextBox id="txtemail" MaxLength="50" CssClass="gunenrtext"  runat="server"></asp:TextBox>
		    <br />
                <p>
					Pin 1
				</p>
				 <asp:TextBox id="txtpin1" MaxLength="20" CssClass="gunenrtext" runat="server" Text="0"></asp:TextBox><br />
                <p>
					Pin 2
				</p>
				 <asp:TextBox id="txtpin2"  MaxLength="20" CssClass="gunenrtext" runat="server" Text="0"></asp:TextBox>
				<!--<input type="radio" id="company_name_toggle_on" name="company_name_toggle_group"></input>
				<label for="company_name_toggle_on">Yes</label>
				&emsp;
				<input type="radio" id="company_name_toggle_off" name="company_name_toggle_group"></input>
				<label for="company_name_toggle_off">No</label>
				
				<div id="company_name_wrap">
					<label for="company_name">
						Company Name:
					</label>
					<input type="text" id="company_name"></input>
				</div>-->
			<!--
				<div class="push">			
					<p>
						Will anyone in your group require special accommodations?
					</p>
					
					<input type="radio" id="special_accommodations_toggle_on" name="special_accommodations_toggle"></input>
					<label for="special_accommodations_toggle_on">Yes</label>
					&emsp;
					<input type="radio" id="special_accommodations_toggle_off" name="special_accommodations_toggle"></input>
					<label for="special_accommodations_toggle_off">No</label>
				</div>
				<div id="special_accommodations_wrap">
					<label for="special_accomodations_text">
						Please explain below:
					</label>
					<textarea rows="10" cols="10" id="special_accomodations_text"></textarea>
				</div>
			-->
			</fieldset>
		
			<fieldset id="step_3">
				<legend>Final Step</legend>
			<p> <asp:TextBox ID="txtterms" runat="server" ReadOnly="True" Rows="2" TextMode="MultiLine" Width="100%" Height="30px" BorderWidth="0" 
    Text="EclickSense Terms of Services
Welcome to EclickSense!

•	Anyone wanting to be a member in EclickSense Program should be at least 18 years of age. Our services are available to the general public who abide by our Terms and Conditions. The use of personal account area of this website is constrained to program members in addition to individuals who are invited by them personally. Every transaction is regarded as a private transaction between our company and its qualified members.
•	Member of EclickSense be in agreement that all the EclickSense, communications and materials acquiredClickSourcerare voluntary and are expected to be kept in private, confidential and protected from any disclosure. 
•	The following conditions should not be ignored for the information, communications and materials given by us:
•	They are not to be taken as an offer or a solicitation for Membership in any jurisdiction which considers non-public offers or solicitation unlawful, nor to any individual to whom it will be unlawful to make such an offer or solicitation. 
•	Using the latest and most advanced technologies, the data provided by program participants will be kept in utmost safety and protected from access by any third party. 
•	The Membership decisions are taken up by you. You are in agreement that the information, communications and materials found on this website are meant to be informative and educational matter and not a Membership advice or solicitation. 
•	The rules, commissions and rates of the program are subject to change irrespective of time and event. Such changes can happen without any prior intimation especially in order to protect the interests of our members.
•	You are in complete agreement that it is your sole responsibility to review the existing terms of service. 
•	EclickSense Company should not be held responsible for any loss, damages and costs resulting from any violation of the conditions and terms mentioned on the website. You have to guarantee to us that you will always abide by the local, national and international laws. 
•	Without any personal experience with us, you are expected not to post any degrading remarks about our company on public forums and on any other independent review websites. At its own discretion, we reserve the right ClickSourcer or decline any member for membership owing no explanation.
•	EclickSense Company have all the rights reservation to check, charge, modify, change any of the terms, schemes, products, benefits, systems, shares and to take any decisions any time without any prior notice regarding this website, company and any members.

Thanks for your patience.
"></asp:TextBox></p>
				<label for="rock">
					Are you ready to rock?(accepted terms and Conditions)
				</label>
				
            <asp:Button ID="Button1"  CssClass="button" runat="server" OnClick="reg_Click" Text="Complete" />
			</fieldset>

		
	
	</div>
    </form>
</body>
</html>
