﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for payment_cs
/// </summary>
public class payment_cs
{
    cs_vars vars = new cs_vars();
    db_payment ob_userinfo = new db_payment();
    string[] tmp = new string[4];
    // int tax_rate = 0;

    public double get_amount(string scheme)
    {
        char[] array = scheme.ToCharArray();
        if (array[0] == 'a')
            return 10000;
        else if (array[0] == 'p')
            return 5000;
        else if (array[0] == 'b')
            return 20000;
        else if (array[0] == 'c')
            return 50000;
        else if (array[0] == 'd')
            return 100000;
        else if (array[0] == 'z')
            return Convert.ToDouble(ob_userinfo.get_var("ultimate_inv_amount"));
        else return 15;
    }
    public int get_year(string scheme)
    {
        char[] array = scheme.ToCharArray();
        if (array.Contains('1'))
            return 1;
        else if (array.Contains('2'))
            return 2;
        else if (array.Contains('3'))
            return 3;
        else if (array.Contains('4'))
            return 5;
        else if (array.Contains('z'))
            return Convert.ToInt16(ob_userinfo.get_var("ultimate_inv_years"));
        else
            return Convert.ToInt16(ob_userinfo.get_var("networker_upgrade_min_amount_years"));

    }
    public void refarral_inc(int id, string scheme)
    {
        //int year = get_year(scheme);
        /*int year = get_year(scheme);
        double tax_rate = ob_userinfo.get_var("govt_service");*/
        double amount = this.get_amount(scheme) * ob_userinfo.get_var("ref_perc");
        if (amount != 0)
            ob_userinfo.insert_ref_payment(id, (amount));

    }
    /*public void bid_inc(int id)
    {
        double amount = 400;
        ob_userinfo.insert_bid_payment1st(id, amount);

    }*/
    public double get_perc(Int32 id, string scheme)
    {
       /* double days_scheme = get_year(scheme) * 365 * 0.2;
        if (get_year(scheme) == 0)
            days_scheme = 5 * 365 * 0.2;
        if (ob_userinfo.get_datediff(id) < days_scheme)
            return ob_userinfo.get_var("roy_1st_20");
        else if (ob_userinfo.get_datediff(id) < (days_scheme * 2))
            return ob_userinfo.get_var("roy_2nd_20");
        else if (ob_userinfo.get_datediff(id) < (days_scheme * 3))
            return ob_userinfo.get_var("roy_3rd_20");
        else if (ob_userinfo.get_datediff(id) < (days_scheme * 4))
            return ob_userinfo.get_var("roy_4th_20");
        else if (ob_userinfo.get_datediff(id) < (days_scheme * 5))
            return ob_userinfo.get_var("roy_5th_20");
        else*/
            return 0;
    }
    public void royalty_inc(int id, string scheme)
    {
      /*double amount = get_amount(scheme) / ob_userinfo.get_var("note2tk");

        tmp = ob_userinfo.get_parents_uid_hand(id);

        for (int i = 0; tmp[i] != null; i = i + 2)
        {
            if (!ob_userinfo.check2dayroyalty(Convert.ToInt16(tmp[i]), tmp[i + 1]))
                ob_userinfo.insert_royalty_calc(Convert.ToInt16(tmp[i]), tmp[i + 1], amount);
            else
                ob_userinfo.update_royalty_calc(Convert.ToInt16(tmp[i]), tmp[i + 1], amount);
        }*/

    }
    public void royalty_main(List<Int16> parents, string scheme)
    {
       /* int i = 0;
        double tax_rate = ob_userinfo.get_var("govt_service");
        //double note = ob_userinfo.get_var("note2tk");
        double income = 0, max_limit = ob_userinfo.get_var("max_match_limit");
        while (true)
        {
            if (parents[i] == 0) break;
            double rmoney = ob_userinfo.get_right_money(parents[i]);
            double lmoney = ob_userinfo.get_left_money(parents[i]);

            if ((rmoney != 0) && (lmoney != 0))
            {
                income = (rmoney - lmoney);
                if (income < 0)
                {
                    //ob_userinfo.update_royalty(parents[i], "L");
                    income = rmoney;

                }
                else
                    income = lmoney;// set income
                if (income >= max_limit)
                {
                    //double tmp = income - max_limit;
                    //                    if (rmoney > lmoney)
                    //ob_userinfo.update_royalty_calc_next(parents[i], income-max_limit);

                    ob_userinfo.update_royalty_calc_next(parents[i], "L", lmoney - max_limit);
                    ob_userinfo.update_royalty_calc_next(parents[i], "R", rmoney - max_limit);
                    income = max_limit;

                }
                income = income * get_perc(vars.get_userid(), scheme);
                double day_royalty_inc = ob_userinfo.get_royaly_inc(parents[i]);
                if (income > day_royalty_inc)
                {
                    ob_userinfo.update_royalty(parents[i], (income * (1 - tax_rate)));

                }
                else if (day_royalty_inc == 0)
                    ob_userinfo.insert_royalty_payment(parents[i], (income * (1 - tax_rate)));

            }
            i++;

        }*/
    }
}