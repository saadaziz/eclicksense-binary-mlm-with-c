﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for gen_db
/// </summary>
public class gen_db
{
    /*static string MyConString = "SERVER=209.172.55.5;" +
               "DATABASE=mlmbd;" +
               "UID=gen_user;" +
               "PASSWORD=123qweasd;Pooling=true;";*/
    static string MyConString = new database_conn().send_constr();
    MySqlConnection connection = new MySqlConnection(MyConString);
    static Int32 userid = 0;
   // static string username_ob;
    public List<string> get_Userinfo_mod(Int32 user_func)
    {
        List<string> row = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select username,fullname,address,doj,lcount,rcount from db_userinfo where uid=" + user_func;
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            row.Add(Convert.ToString(Reader1["username"]));
            row.Add(Convert.ToString(Reader1["fullname"]));
            row.Add(Convert.ToString(Reader1["address"]));
            row.Add(Convert.ToString(Reader1["doj"]));
            row.Add(Convert.ToString(Reader1["lcount"]));
            row.Add(Convert.ToString(Reader1["rcount"]));
            break;
        }
        connection.Close();///
        return row;
    }
    public Int16 getLcount(Int32 userid)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  lcount from db_userinfo where uid = " + userid + ";";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["lcount"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 getRcount(Int32 userid)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  rcount from db_userinfo where uid = " + userid + ";";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["rcount"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 get_parent(Int32 userid_func)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "select ref_id from db_refrs where uid ='" + userid_func + "';";
        Reader1 = command.ExecuteReader();

        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["ref_id"]);
            break;
        }
        connection.Close();
        return count;
    }
    public Int16 get_min_userid()
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  min(uid) from db_userinfo;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["min(uid)"]);
            break;
        }
        connection.Close();///
        return count;
    }
    
    public double get_left_money(Int32 userid_func)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select amount from db_royalty where uid ='" + userid_func + "' and hand = 'L' and datediff(now(),time)<1;";
        Reader1 = command.ExecuteReader();
         while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["amount"]);
            break;
        }
        connection.Close();///
        return count;
    }

    public double get_right_money(Int32 userid_func)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select amount from db_royalty where uid ='" + userid_func + "' and hand = 'R' and datediff(now(),time)<1;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["amount"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public double get_left_money_all(Int32 userid_func)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select sum(amount) from db_royalty where uid ='" + userid_func + "' and hand = 'L' group by uid ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["sum(amount)"]);
            break;
        }
        connection.Close();///
        return count;
    }

    public double get_right_money_all(Int32 userid_func)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select sum(amount) from db_royalty where uid ='" + userid_func + "' and hand = 'R' group by uid ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["sum(amount)"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 get_lchild(Int32 userid_func)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select uid from db_refrs where ref_id ='" + userid_func + "' and hand = 'l';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["uid"]);
            break;
        }
        connection.Close();///
        return count;
    }

    public string get_scheme(Int32 id)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select scheme from db_epins where uid ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["scheme"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public List<Int32> get_spon_list(Int32 id,string hand)
    {
        List<Int32> log = new List<Int32>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_sponsors where spon_id ='" + id + "' and hand = '"+hand+"';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log.Add(Convert.ToInt32(Reader["uid"]));
            //break;
        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public List<Int32> get_child_list(Int32 id)
    {

        List<Int32> log = new List<Int32>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select child_id from db_parent where uid ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log.Add(Convert.ToInt32(Reader["child_id"]));
            //break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public List<Int32> get_child_list_up(Int32 id)
    {

        List<Int32> log = new List<Int32>();
        for (int i = 0; i < 16; i++)
        {
            log.Add(0);
        }
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        connection.Open();
       
        command.CommandText = "select child_id,position from db_parent where uid =" + id + " and  position <16 and position!=0 order by position ;";

        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log[Convert.ToInt16(Reader["position"])-2]=Convert.ToInt32(Reader["child_id"]);
            //break;

        }

        Reader.Close();
        
        connection.Close();

        return log;
    }
    public List<Int32> get_spon_list(Int32 id)
    {

        List<Int32> log = new List<Int32>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_sponsors where spon_id ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log.Add(Convert.ToInt32(Reader["uid"]));
            //break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public string[] get_Userinfo(Int32 user_func)
    {
        string[] count = new string[8];
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select fullname,address,email,DATE_FORMAT(DOJ ,'%e-%M-%Y' ),spon_id from db_userinfo,db_sponsors where db_userinfo.uid ='" + user_func + "' and db_sponsors.uid="+user_func+";";
        Reader1 = command.ExecuteReader();
        int i = 0;
        while (Reader1.Read())
        {
            count[i] = Convert.ToString(Reader1["fullname"]);
            i++;
            count[i] = Convert.ToString(Reader1["DATE_FORMAT(DOJ ,'%e-%M-%Y' )"]);
            i++;
            count[i] = Convert.ToString(Reader1["spon_id"]);
            i++;

            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 get_rchild(Int32 userid_func)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select uid from db_refrs where ref_id ='" + userid_func + "' and hand = 'r';";
        Reader1 = command.ExecuteReader();
         while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["uid"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Boolean checkchild(Int32 userid,Int32 child)
    {
        Boolean count = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select pid from db_parent where uid =" + userid + " and child_id = "+child+";";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = true;
            break;
        }
        connection.Close();///
        return count;
    }
}

