﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for db_freelance
/// </summary>
public class db_freelance
{
    static string MyConString = new database_conn().send_constr("freelance");
    MySqlConnection connection = new MySqlConnection(MyConString);
    
    public Boolean checkLogin(string uname, string pass)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select password from job_reg where email ='" + uname + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (pass == Convert.ToString(Reader["password"]))
            {

                log = true;
                /*userid = Convert.ToInt32(Reader["uid"]);
                username_ob = uname;*/
                break;
            }
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public void insertuser(string pass, string email, string dob, string full, string contact,string country)
    {
        pass = this.replace_inject(pass);
        if(dob!=null)
        {
            string[] dateofbirth = dob.Split('/');
            dob = dateofbirth[2] + "-" + dateofbirth[0] + "-" + dateofbirth[1];
        }
        string cmd = "insert into job_reg (email,password,name,dob,phone,country)" +
                    "values('" + email + "','" + pass + "','" + this.replace_inject(full) + "','" + dob + "','" + this.replace_inject(contact) + "','"+country+"')";

        CreateMySqlCommand(cmd, connection);
        //this.insert_username(email);
    }
    public string replace_inject(string data)
    {
        data = data.Replace("'", "&#39");
        data = data.Replace("\"", "&#34");
        return data;
    }
    public void CreateMySqlCommand(string myExecuteQuery, MySqlConnection myConnection)
    {
        MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, myConnection);
        myCommand.Connection.Open();
        //myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public Boolean checkemail(string email)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select email from job_reg where email ='" + email + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = true;
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Int32 get_skill_count()
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(DISTINCT(TRIM( skill))) as dis_skill from job_skill where CHARACTER_LENGTH(skill)<10;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["dis_skill"]);
        }
        connection.Close();///
        return count;
    }
    public Int32 get_job_count()
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count( job_id) as dis_skill from job_detail;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["dis_skill"]);
        }
        connection.Close();///
        return count;
    }
    public Int32 get_cat_count()
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(DISTINCT(TRIM( catagory))) as dis_cat from job_catagory  where catagory  is not null order by LENGTH(catagory) ASC;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["dis_cat"]);
        }
        connection.Close();///
        return count;
    }
    public Int32 get_scat_count(string scat)
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(( cat_id)) as dis_cat from job_catagory where subcatagory= '" + scat + "' ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["dis_cat"]);
        }
        connection.Close();///
        return count;
    }	
    public List<string> get_distinct_skill()
    {

        List<string> skill = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT distinct(trim(skill)) as distin from job_skill where CHARACTER_LENGTH(skill)<10 ORDER BY ascii( skill) asc";
        Reader1 = command.ExecuteReader();
        // int i = 0;
        while (Reader1.Read())
        {
            skill.Add(Convert.ToString(Reader1["distin"]));
            //i++;
        }
        connection.Close();///
        return skill;
    }
    public Int16 get_userid(string email)
    {

        Int16 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select userid from job_reg where email ='" + email + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt16(Reader["eclicksense"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public List<string> get_distinct_subcat(string cat)
    {

        List<string> subcat = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT distinct(trim(subcatagory)) as distin from job_catagory where catagory = '"+cat+"' limit 0, 15";
        Reader1 = command.ExecuteReader();
        // int i = 0;
        while (Reader1.Read())
        {
            subcat.Add(Convert.ToString(Reader1["distin"]));
            //i++;
        }
        connection.Close();///
        return subcat;
    }
    public List<string> get_distinct_cat()
    {

        List<string> cat = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT distinct(trim(catagory)) as distin from job_catagory where catagory  is not null order by LENGTH(catagory) ASC;";
        Reader1 = command.ExecuteReader();
        // int i = 0;
        while (Reader1.Read())
        {
            cat.Add(Convert.ToString(Reader1["distin"]));
            //i++;
        }
        connection.Close();///
        return cat;
    }
    public Int32 get_skill_count(string skill_name)
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(distinct((skill))) as dis_skill FROM job_skill where skill like '%"+skill_name+"%' ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["dis_skill"]);
        }
        connection.Close();///
        return count;
    }
    public string get_description(Int32 jobid)
    {

        string count ="";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT description FROM job_description where job_id1= " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToString(Reader1["description"]);
        }
        connection.Close();///
        return count;
    }
    public string get_title(Int32 jobid)
    {

        string count = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT job_title FROM job_detail where job_id = " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToString(Reader1["job_title"]);
        }
        connection.Close();///
        return count;
    }
    public string get_link(Int32 jobid)
    {

        string count = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT linkaddr FROM job_url where job_id1 = " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToString(Reader1["linkaddr"]);
        }
        connection.Close();///
        return count;
    }
    public List<string> get_sitename(Int32 jobid)
    {

        List<string> count = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT sitename,linkaddr FROM job_url where job_id1 = " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count.Add( Convert.ToString(Reader1["sitename"]));
            count.Add(Convert.ToString(Reader1["linkaddr"]));

        }
        connection.Close();///
        return count;
    }
    
    public List< string> get_budget(Int32 jobid)
    {

        List<string> count = new List< string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT budget,delivery FROM job_fixed where job_id1 = " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count.Add( Convert.ToString(Reader1["budget"]));

            count.Add(Convert.ToString(Reader1["delivery"]));
        }
        connection.Close();///
        return count;
    }
    public List<string> get_job_det(Int32 jobid)
    {

        List<string> count = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT job_title,job_type,started FROM job_detail where job_id = " + jobid + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count.Add(Convert.ToString(Reader1["job_title"]));
            count.Add(Convert.ToString(Reader1["job_type"]));
            count.Add(Convert.ToString(Reader1["started"]));

        }
        connection.Close();///
        return count;
    }
}