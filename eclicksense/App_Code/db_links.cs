﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for db_links
/// </summary>
public class db_links
{
    static string strConnString = new database_conn().send_constr("click");
    MySqlConnection connection = new MySqlConnection(strConnString);
    MySqlConnection objConn;
    MySqlCommand objCmd;
    public string insert_links_rev(string[] links)
    {
        string gg1 = null;
        gg1 = "values ";

        foreach (string gg in links)
        {
            if (!links.Equals(""))
                gg1 += "('" + gg + "'),";
        }
        gg1 = gg1.Remove(gg1.LastIndexOf(","), 1);
        gg1 += ";";
        string cmd = "insert into tb_linklist_rev (link) " + gg1;


        CreateMySqlCommand(cmd, connection);
        return "success";
    }
    public void send_click_amount(Int32 sender, Int32 recvr, double amount)
    {
        //string date = DateTime.Now.ToString("yyyy/MM/dd");
        string cmd = null;
        cmd = "INSERT INTO tb_userclick_updated (uid,counts,dated) VALUES (" + sender + "," + amount * (-1) + ",date(now()));";
        CreateMySqlCommand(cmd, connection);
        cmd = "INSERT INTO tb_userclick_updated (uid,counts,dated) VALUES (" + recvr + "," + amount + ",date(now()));";

        CreateMySqlCommand(cmd, connection);

    }
    public void send_click_amount(Int32 sender, double amount)
    {
        //string date = DateTime.Now.ToString("yyyy/MM/dd");
        string cmd = null;
        cmd = "INSERT INTO tb_userclick_updated (uid,counts,dated) VALUES (" + sender + "," + amount * (-1) + ",date(now()));";
        CreateMySqlCommand(cmd, connection);


    }
    public string insert_links(string[] links)
    {
        string gg1 = null;
        gg1 = "values ";

        foreach (string gg in links)
        {
            if (!links.Equals(""))
                gg1 += "('" + gg + "'),";
        }
        gg1 = gg1.Remove(gg1.LastIndexOf(","), 1);
        gg1 += ";";
        string cmd = "insert into tb_linklist (link) " + gg1;


        CreateMySqlCommand(cmd, connection);
        return "success";
    }
    public Int32 get_link_id()
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT l_id FROM tb_linklist order by rand() limit 0,1;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt32(Reader["l_id"]);
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public Int32 get_sum_amount(Int32 userid)
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT sum(counts) as count1 FROM tb_userclick_updated WHERE uid=" + userid + " ;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if ((Reader["count1"]) != DBNull.Value)
                log = Convert.ToInt32(Reader["count1"]);
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }

    public Int32 get_link_id_rev()
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT l_id FROM tb_linklist_rev order by rand();";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt32(Reader["l_id"]);
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public void insert_click(Int32 uid)
    {
        //string date = DateTime.Now.ToString("yyyy/MM/dd");
        string cmd = null;
        if (!isavail(uid))
            cmd = "INSERT INTO tb_userclick_updated (uid,counts,dated) VALUES (" + uid + ",1,date(now()));";
        else
            cmd = "update tb_userclick_updated set counts = counts +1 where uid = " + uid + " and dated=date(now());";

        CreateMySqlCommand(cmd, connection);

    }
    public void CreateMySqlCommand(string myExecuteQuery, MySqlConnection myConnection)
    {
        MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, myConnection);
        myCommand.Connection.Open();
        //myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public bool isavail(Int32 uid)
    {

        bool gg = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select id from tb_userclick_updated where uid = " + uid + " and dated = date(now());";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            gg = true;
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return gg;
    }
    public Int32 get_count_2day(Int32 uid)
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select counts from tb_userclick_updated where uid ='" + uid + "' and datediff(now(),dated)<1;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt32(Reader["counts"]);
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public string get_linkurl_rev(int id)
    {

        string log = "";
        //int lid = Convert.ToInt32(id);
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select link from tb_linklist_rev where l_id = " + id + " ;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["link"]);
            break;
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;


    }
    public string get_linkurl(int id)
    {
        
            string log = "";
            //int lid = Convert.ToInt32(id);
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            command.CommandText = "select link from tb_linklist where l_id = "+id+" ;";
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                log = Convert.ToString(Reader["link"]);
                break;
            }
            Reader.Close();
            // get_user_id(uname);
            connection.Close();

            return log;
        
        
    }
    public Int32 get_date_diff(string date1,string date2)
    {
        if (date1 != null)
        {
            Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            command.CommandText = "select DATEDIFF('"+date1+"','"+date2+"') as counts ;";
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                log = Convert.ToInt32(Reader["counts"]);
                break;
            }
            Reader.Close();
            // get_user_id(uname);
            connection.Close();

            return log;
        }
        else
            return 0;
    }
}