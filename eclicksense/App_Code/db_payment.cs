﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for db_payment
/// </summary>
public class db_payment
{
    /*static string MyConString = "SERVER=209.172.55.5;" +
              "DATABASE=mlmbd;" +
              "UID=Gunner;" +
              "PASSWORD=123qweasd;Pooling=true;";*/

    static string MyConString = new database_conn().send_constr();
    MySqlConnection connection = new MySqlConnection(MyConString);
    static Int32 userid = 0;
   // static string username_ob;
    public void convert_click(int uid, double notes)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','Converted Click Income')";

        CreateMySqlCommand(cmd, connection);

    }
    public void insert_ref_payment(int uid, double notes)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','Referral Income');";

        CreateMySqlCommand(cmd, connection);

    }
    public void insert_bid_payment(int uid, double notes)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','bid wallet')";

        CreateMySqlCommand(cmd, connection);
        notes = -1 * notes;
        cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','Investment Income')";

        CreateMySqlCommand(cmd, connection);

    }
    public double get_var(string var)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select value from db_variables where variable ='" + var + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["value"]);
            break;
        }
        connection.Close();///
        return count;
    }

    public Int32 get_count_page(Int32 var)
    {
        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(pid) as value from db_payment where uid = " + var + " ;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["value"]);
            break;
        }
        connection.Close();///
        return count/10;
    }
    public string get_scheme(Int32 id)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select scheme from db_epins where uid ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["scheme"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }

    public void insert_convert_note(int uid, double notes, int type)
    {
        string tpe = null, cmd = null;
        if (notes != 0)
        {
            if (type == 1)
            {
                tpe = "Converted Royalty Income";
                cmd = "insert into db_payment (uid,notes,type)" +
                        "values('" + uid + "','-" + notes + "','" + tpe + "')";

                CreateMySqlCommand(cmd, connection);
                tpe = "Converted Investor Income";
                cmd = "insert into db_payment (uid,notes,type)" +
                        "values('" + uid + "','" + notes + "','" + tpe + "')";

                CreateMySqlCommand(cmd, connection);
            }
            else
            {
                tpe = "Converted Royalty Income";
                cmd = "insert into db_payment (uid,notes,type)" +
                        "values('" + uid + "','" + notes + "','" + tpe + "')";

                CreateMySqlCommand(cmd, connection);
                tpe = "Converted Investor Income";
                cmd = "insert into db_payment (uid,notes,type)" +
                        "values('" + uid + "','-" + notes + "','" + tpe + "')";

                CreateMySqlCommand(cmd, connection);
            }
        }
    }
    public void insert_xferred_investor_payment(int uid, double notes)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','Transferred Investor Income')";

        CreateMySqlCommand(cmd, connection);

    }
    public void send_msg(Int32 id_to, Int32 id_from, string msg)
    {
        msg = this.replace_inject(msg);
        string cmd = "insert into db_messages (userid_from,userid_to,msg)" +
                    "values('" + id_from + "','" + id_to + "','" + msg + "')";

        CreateMySqlCommand(cmd, connection);

    }
    public string replace_inject(string data)
    {
        data = data.Replace("'", "&#39");
        data = data.Replace("\"", "&#34");
        return data;
    }
    public void insert_xferred_royalty_payment(int uid, double notes)
    {
        if (notes != 0)
        {
            string cmd = "insert into db_payment (uid,notes,type)" +
                     "values('" + uid + "','" + notes + "','Transferred Royalty Income')";

            CreateMySqlCommand(cmd, connection);
        }
    }

    public Boolean checkpin(int userid, Int32 pin)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select transaction from db_userinfo where uid ='" + userid + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (pin == Convert.ToInt32(Reader["transaction"]))
            {

                log = true;
                /*userid = Convert.ToInt32(Reader["uid"]);
                username_ob = uname;*/
                break;
            }
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public double sum_payment(int userid)
    {

        double log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT sum(notes) from db_payment where uid ="+userid+";";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
           if(Reader["sum(notes)"]!=  DBNull.Value)
                log = Convert.ToDouble(Reader["sum(notes)"]);
                /*userid = Convert.ToInt32(Reader["uid"]);
                username_ob = uname;*/
                break;
            
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public void CreateMySqlCommand(string myExecuteQuery, MySqlConnection myConnection)
    {
        MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, myConnection);
        myCommand.Connection.Open();
        //myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
}