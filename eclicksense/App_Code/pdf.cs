﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using sharpPDF;
using sharpPDF.Enumerators;
/// <summary>
/// Summary description for pdf
/// </summary>
public class pdf
{

    public void viewpdf(string line1, string line2, string line3, string line4, string line5, string line6, Int32 id)
    {

        pdfDocument myDoc = new pdfDocument("TUTORIAL", "ME");
        pdfPage myPage = myDoc.addPage();
        /*Use the directly predefined Colors[Deprecated]*/
        myPage.addText(line1, 80, 700, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("Welcome to our ClickSource.com  Community.", 80, 660, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("We are pleased to have you as our honorable member.", 80, 650, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("Please read carefully and keep the personal information mentioned below:", 80, 620, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText(line2, 80, 580, myDoc.getFontReference(predefinedFont.csHelvetica), 10);
        myPage.addText(line3, 80, 560, myDoc.getFontReference(predefinedFont.csHelvetica), 10);
        myPage.addText(line4, 80, 540, myDoc.getFontReference(predefinedFont.csHelvetica), 10);
        myPage.addText(line5, 80, 520, myDoc.getFontReference(predefinedFont.csHelvetica), 10);
        myPage.addText(line6, 80, 500, myDoc.getFontReference(predefinedFont.csHelvetica), 10);
        myPage.addText("N.B.: You can change your Password later from the settings page after login.", 80, 470, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("For any technical support, write your inquiry to : support@clicksource.com", 110, 450, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("Be with us, we are for changing the world.", 80, 420, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("Thanks & Regards,", 80, 380, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("ClickSourcer.com Management", 80, 350, myDoc.getFontReference(predefinedFont.csHelvetica), 12);
        myPage.addText("'We are for World, World is for Us'", 220, 310, myDoc.getFontReference(predefinedFont.csHelvetica), 13, new pdfColor(112, 27, 184));


        myDoc.createPDF(System.Web.HttpContext.Current.Server.MapPath("pdfs1") + "\\eclick" + id + ".pdf");
        myPage = null;
        myDoc = null;
    }

}
