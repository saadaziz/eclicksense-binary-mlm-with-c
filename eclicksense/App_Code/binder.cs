﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

using System.Web.UI.WebControls;
using System.Data;
/// <summary>
/// Summary description for binder
/// </summary>
public class binder
{
   /* static string strConnString = "SERVER=209.172.55.5;" +
              "DATABASE=mlmbd;" +
              "UID=Gunner;" +
              "PASSWORD=123qweasd;Pooling=true;";*/
    static string strConnString = new database_conn().send_constr("192.168.65.59");
    MySqlConnection connection = new MySqlConnection(strConnString);
    MySqlConnection objConn;

    MySqlCommand objCmd;
    public void BindData(string strsql, Repeater r1)
    {
        objConn = new MySqlConnection(strConnString);
        objConn.Open();
        MySqlDataReader dtReader;
        objCmd = new MySqlCommand(strsql, objConn);
        dtReader = objCmd.ExecuteReader();
        //*** BindData to Repeater ***//
        r1.DataSource = dtReader;
        r1.DataBind();


        dtReader.Close();
        dtReader = null;
        objConn.Close();
    }
    public void create_xls(string sql)
    {
        DataSet ds = new DataSet("New_DataSet");
        DataTable dt = new DataTable("New_DataTable");

        //Set the locale for each
        ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
        dt.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;

        //Open a DB connection (in this example with OleDB)
        objConn = new MySqlConnection(strConnString);
        objConn.Open();

        //Create a query and fill the data table with the data from the DB

        MySqlCommand cmd = new MySqlCommand(sql, objConn);
        MySqlDataAdapter adptr = new MySqlDataAdapter();

        adptr.SelectCommand = cmd;
        adptr.Fill(dt);
        objConn.Close();

        //Add the table to the data set
        ds.Tables.Add(dt);

        //Here's the easy part. Create the Excel worksheet from the data set
        ExcelLibrary.DataSetHelper.CreateWorkbook(System.Web.HttpContext.Current.Server.MapPath("pdfs1")+"\\MyExcelFile"+new cs_vars().get_userid().ToString()+".xls", ds);
        

    }
    public void BindData(string strsql, GridView r1)
    {
        objConn = new MySqlConnection(strConnString);
        objConn.Open();
        MySqlDataReader dtReader;
        objCmd = new MySqlCommand(strsql, objConn);
        dtReader = objCmd.ExecuteReader();
        //*** BindData to Repeater ***//
        r1.DataSource = dtReader;
        r1.DataBind();


        dtReader.Close();
        dtReader = null;
        objConn.Close();
    }
    public void BindData(string strsql, Repeater r1, string click)
    {
        if (click.Equals("click"))
            objConn = new MySqlConnection(new database_conn().send_constr("click"));
        else
            objConn = new MySqlConnection(new database_conn().send_constr("freelance"));
        objConn.Open();
        MySqlDataReader dtReader;
        objCmd = new MySqlCommand(strsql, objConn);
        dtReader = objCmd.ExecuteReader();
        //*** BindData to Repeater ***//
        r1.DataSource = dtReader;
        r1.DataBind();


        dtReader.Close();
        dtReader = null;
        objConn.Close();
    }

    public void BindData(string strsql, GridView r1,string click)
    {
        if(click.Equals("click"))
            objConn = new MySqlConnection(new database_conn().send_constr("click"));
        else
            objConn = new MySqlConnection(new database_conn().send_constr("freelance"));
        objConn.Open();
        MySqlDataReader dtReader;
        objCmd = new MySqlCommand(strsql, objConn);
        dtReader = objCmd.ExecuteReader();
        //*** BindData to Repeater ***//
        r1.DataSource = dtReader;
        r1.DataBind();


        dtReader.Close();
        dtReader = null;
        objConn.Close();
    }

}