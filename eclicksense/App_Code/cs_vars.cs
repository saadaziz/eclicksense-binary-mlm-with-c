﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;

public class cs_vars
{
    static Int64 admin = 0;
    Int32 userid = 0;
    Int32 root = 0;
    static string rand = null;

    public string get_scheme_no(int amount, int year)
    {
        year = 4;
        if (amount == 1)
            return "p" + year.ToString();
        else if (amount == 2)
            return "a" + year.ToString();
        
        else
            return "x";

    }
    public string get_scheme_name(string scheme)
    {
        if (scheme != null)
        {
            if (scheme.Equals("a1"))
                return "1 Year 2,000Note";
            else if (scheme.Equals("a2"))
                return "2 Years 2,000Note";
            else if (scheme.Equals("a3"))
                return "3 Years 2,000Note";
            else if (scheme.Equals("a4"))
                return "5 Years 2,000Note";
            else if (scheme.Equals("b1"))
                return "1 Year 4,000Note)";
            else if (scheme.Equals("b2"))
                return "2 Years 4,000Note";
            else if (scheme.Equals("b3"))
                return "3 Years 4,000Note";
            else if (scheme.Equals("b4"))
                return "5 Years 4,000Note";
            else if (scheme.Equals("c1"))
                return "1 Year 10,000Note";
            else if (scheme.Equals("c2"))
                return "2 Years 10,000Note";
            else if (scheme.Equals("c3"))
                return "3 Years 10,000Note";
            else if (scheme.Equals("c4"))
                return "5 Years 10,000Note";
            else if (scheme.Equals("d1"))
                return "1 Year 20,000Note ";
            else if (scheme.Equals("d2"))
                return "2 Years 20,000Note ";
            else if (scheme.Equals("d3"))
                return "3 Years 20,000Note ";
            else if (scheme.Equals("d4"))
                return "5 Years 20,000Note ";
            else if (scheme.Equals("p1"))
                return "1 Year 1,000Note";
            else if (scheme.Equals("p2"))
                return "2 Years 1,000Note";
            else if (scheme.Equals("p3"))
                return "3 Years 1,000Note";
            else if (scheme.Equals("p4"))
                return "5 Years 1,000Note";
            else if (scheme.Equals("y"))
                return "Upgraded Networking";
            else if (scheme.Equals("x"))
                return "Premium Member";
            else if (scheme.Equals("z"))
                return "Ultimate Investor";

        }
        return null;
    }
    public void set_admin(Int64 chk)
    {
        admin = chk;

    }
    public void set_rand(string chk)
    {
        rand = chk;

    }
    public void set_userid(Int32 chk)
    {
        HttpContext.Current.Session.Add("eclick", Convert.ToString(1.0));
        userid = chk;
        set_root(chk);

    }
    public void set_root(Int32 chk)
    {
        root = chk;

    }
    public Int64 get_admin()
    {
        return admin;

    }
    public Int32 get_userid()
    {
        //return userid;
        string userid_str = (string)(HttpContext.Current.Session["eclicksense"]);
        return Convert.ToInt32(userid_str);
    }
    public Int32 get_root()
    {
        string userid_str = (string)(HttpContext.Current.Session["root"]);
        return Convert.ToInt32(GetNumberFromStrFaster( userid_str));

    }
    public string get_rand()
    {
        return rand;

    }
    public string GetNumberFromStrFaster(string str)
    {
        if (str != null)
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
        else
            return this.get_userid().ToString();
    }
   

}
