﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
/// <summary>
/// Summary description for get_child
/// </summary>
public class get_child
{
    cs_vars vars = new cs_vars();
    gen_db ob_userinfo = new gen_db();
    public List<Int32> get_child_list(Int32 userid)
    {

        List<Int32> child_lst = new List<Int32>();
        child_lst.Add(userid);
        int chld_lstcount = 1;
        for (Int32 i = 0; i < chld_lstcount; i++)
        {
            Int32 lchld = ob_userinfo.get_lchild(child_lst[i]);
            Int32 rchld = ob_userinfo.get_rchild(child_lst[i]);
            if (lchld != 0)
                child_lst.Add(lchld);
            
            if (rchld != 0)
                child_lst.Add(rchld);
            chld_lstcount = child_lst.Count;
            if (chld_lstcount == 6) break;
        }
        if (child_lst.Count != 6)
            for (int j = child_lst.Count; j < 7; j++)
                child_lst.Add(0);
        return child_lst;
    }
    public List<Int32> get_child_list_gen(Int32 userid)
    {

        List<Int32> child_lst = new List<Int32>();
        child_lst.Add(userid);
        int chld_lstcount = 1;
        for (Int32 i = 0; i < chld_lstcount; i++)
        {
            Int32 lchld = ob_userinfo.get_lchild(child_lst[i]);
            Int32 rchld = ob_userinfo.get_rchild(child_lst[i]);
            if (lchld != 0)
                child_lst.Add(lchld);
            else
                child_lst.Add(0);
            if (rchld != 0)
                child_lst.Add(rchld);
            else
                child_lst.Add(0);
            chld_lstcount = child_lst.Count;
            if (i == 6) break;
        }
       /* if (child_lst.Count != 6)
            for (int j = child_lst.Count; j < 7; j++)
                child_lst.Add(0);*/
        return child_lst;
    }
    /*public List<int> get_child_list_updated(Int32 userid)
    {

        Dictionary<Int32, Int64> log = new gen_db().get_child_list_up(userid);


        return log;
    }*/
}
