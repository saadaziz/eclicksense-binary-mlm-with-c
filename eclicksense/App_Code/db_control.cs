﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for db_control
/// </summary>
public class db_control
{
    /*static string MyConString = "SERVER=209.172.55.5;" +
               "DATABASE=mlmbd;" +
               "UID=Gunner;" +
               "PASSWORD=123qweasd;Pooling=true;";*/
    static string MyConString = new database_conn().send_constr();
    MySqlConnection connection = new MySqlConnection(MyConString);
   // static Int32 userid = 0;
    //static string username_ob;
    public List<Int16> get_userid_all(string email)
    {

        List<Int16> log = new List<Int16>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_userinfo where email ='" + email + "' order by doj desc;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log.Add(Convert.ToInt16(Reader["uid"]));
            //break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public void insert_epins_byuser(int epin, int ecard, string scheme, int uid)
    {
        string user = this.get_username(uid);
        string cmd = "insert into db_epins (epin_no,ecard_no,scheme,uid,created_by)" +
              "values('" + epin + "','" + ecard + "','" + scheme + "','" + 0 + "','" + user + "')";

        CreateMySqlCommand(cmd, connection);

    }
    
    public Boolean checkepin(Int32 epin, Int32 ecard, Int32 uid)
    {
        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select eid from db_epins where epin_no ='" + epin + "' and ecard_no='" + ecard + "' and uid = '" + uid + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {

            log = true;
            break;

        }
        Reader.Close();
        connection.Close();

        return log;


    }
    public Int16 getLcount(Int32 userid)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  lcount from db_userinfo where uid = " + userid + ";";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["lcount"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 getRcount(Int32 userid)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  rcount from db_userinfo where uid = " + userid + ";";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["rcount"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public double get_total_income(Int32 id)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT SUM( Notes ) FROM db_payment WHERE uid ='" + id + "' and type!='bid wallet' group by uid;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["SUM( Notes )"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int32 get_count_epin(string id)
    {
        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT count( eid ) as cnt FROM db_epins WHERE created_by ='" + id + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["cnt"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 get_suserid_uname(string username)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT uid as cnt FROM db_userinfo WHERE username ='" + username + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["cnt"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public void insert_uid_into_epin(string mail, Int32 epin, Int32 card)
    {
        string cmd = "update db_epins set uid =(select uid from db_userinfo where email = '" + mail + "' order by DOJ desc limit 0,1),sold='yes' where epin_no = '" + epin + "' and ecard_no='" + card + "' and uid=0;";

        CreateMySqlCommand(cmd, connection);

    }
    public void insert_code_investor_payment(int uid, double notes)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','Pin Generated')";

        CreateMySqlCommand(cmd, connection);

    }
    public Int16 get_userid(string email)
    {

        Int16 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_userinfo where email ='" + email + "' order by doj desc;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt16(Reader["uid"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }

    public void insert_sponsor(Int32 uid, int refid, string lr)
    {
        string cmd = null;

        if (lr == "L")
            cmd = "insert into db_sponsors (uid,spon_id,hand) values('" + uid + "','" + refid + "','L');";
        else
            cmd = "insert into db_sponsors (uid,spon_id,hand) values('" + uid + "','" + refid + "','R');";

        CreateMySqlCommand(cmd, connection);

    }

    public Int16 get_most(Int16 id, string hand)
    {

        Int16 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_refrs where ref_id ='" + id + "' and hand = '" + hand + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt16(Reader["uid"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public void insert_reference(Int32 uid, int refid, string lr)
    {
        string cmd = null;

        if (lr == "L")
            cmd = "insert into db_refrs (uid,ref_id,hand) values('" + uid + "','" + refid + "','L');";
        else
            cmd = "insert into db_refrs (uid,ref_id,hand) values('" + uid + "','" + refid + "','R');";

        CreateMySqlCommand(cmd, connection);

    }
    public double get_var(string var)
    {
        double count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select value from db_variables where variable ='" + var + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToDouble(Reader1["value"]);
            break;
        }
        connection.Close();///
        return count;
    }
    public string replace_inject(string data)
    {
        data = data.Replace("'", "&#39");
        data = data.Replace("\"", "&#34");
        return data;
    }

    public Int16 get_parent(Int32 userid_func)
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "select ref_id from db_refrs where uid ='" + userid_func + "';";
        Reader1 = command.ExecuteReader();
      //  int i = 0;
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["ref_id"]);
            break;
        }
        connection.Close();
        return count;
    }
    public void insert_username(string email)
    {
        Int16 uid = this.get_userid(email);
        string cmd = "update db_userinfo set username =concat('eclick','" + uid + "') where uid = '" + uid + "'";

        CreateMySqlCommand(cmd, connection);

    }
    public void insertuser(string pass, string address, string email, string dob, string full, string contact, string username)
    {
        pass = this.replace_inject(pass);
        username = this.replace_inject(username);
        string[] dateofbirth = dob.Split('/');
        dob = dateofbirth[2] + "-" + dateofbirth[0] + "-" + dateofbirth[1];
        string cmd = "insert into db_userinfo (email,address,password,fullname,dob,Contact,username)" +
                    "values('" + email + "','" + this.replace_inject(address) + "','" + pass + "','" + this.replace_inject(full) + "','" +dob+ "','" + this.replace_inject(contact) + "','"+username+"');";

        CreateMySqlCommand(cmd, connection);
        //this.insert_username(email);
    }
    public Boolean checkuser(int user_id)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select email from db_userinfo where uid ='" + user_id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = true;
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Boolean checkemail(string email)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select email from db_userinfo where email ='" + email + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = true;
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Boolean checkuser(string email)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_userinfo where username ='" + email + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = true;
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public void insert_trans(Int32 pin, Int32 userid)
    {

        string cmd = "update db_userinfo set transaction = '" + pin + "' where uid = " + userid + ";";


        CreateMySqlCommand(cmd, connection);

    }
    public void update_password(string password, Int32 userid)
    {

        string cmd = "update db_userinfo set password = '" + password + "' where uid = " + userid + ";";


        CreateMySqlCommand(cmd, connection);

    }
    public void update_user_email(Int32 id, string email)
    {
        string cmd = "UPDATE db_userinfo SET  email =  '" + email + "' WHERE  uid ='" + id + "';";
        CreateMySqlCommand(cmd, connection);

    }
    public void update_fullName(string fullname, Int32 userid)
    {

        string cmd = "update db_userinfo set fullname = '" + fullname + "' where uid = " + userid + ";";
        CreateMySqlCommand(cmd, connection);

    }
    public void update_contact(string contact, Int32 userid)
    {

        string cmd = "update db_userinfo set contact = '" + contact + "' where uid = " + userid + ";";


        CreateMySqlCommand(cmd, connection);

    }

    public void update_user_pass(int id, string newpass)
    {
        string cmd = "UPDATE db_userinfo SET  password =  '" + newpass + "' WHERE  uid ='" + id + "';";
        CreateMySqlCommand(cmd, connection);

    }
    public void insert_contacts(int id, string gmail, string facebook, string yahoo, string skype)
    {
        string cmd = "insert into db_usercontact values(" + id + ",'" + gmail + "', '" + yahoo + "','" + skype + "','" + facebook + "');";
        CreateMySqlCommand(cmd, connection);

    }
    public string get_pass(Int32 email)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select password from db_userinfo where uid ='" + email + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["password"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public string get_username(Int32 userid)
    {
        if (userid == 0)
            return "";
        else
        {
            string log = "";
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            command.CommandText = "select username from db_userinfo where uid ='" + userid + "';";
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                log = Convert.ToString(Reader["username"]);
                break;

            }

            Reader.Close();
            connection.Close();

            return log;
        }
    }
    public List<string> get_contactinfo(Int32 userid)
    {

        List<string> log = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select * from db_usercontact where uid ='" + userid + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log.Add( Convert.ToString(Reader["gmail"]));
            log.Add(Convert.ToString(Reader["yahoo"]));
            log.Add(Convert.ToString(Reader["skype"]));
            log.Add(Convert.ToString(Reader["facebook"]));
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public void update_address(string address, Int32 userid)
    {
        string cmd = "update db_userinfo set address = '" + address + "' where uid = " + userid + ";";
        CreateMySqlCommand(cmd, connection);

    }
    public string getemail_addr(int id)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select email from db_userinfo where uid ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["email"]);
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Boolean checkLogin(string uname, string pass)
    {

        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select password from db_userinfo where username ='" + uname + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (pass == Convert.ToString(Reader["password"]))
            {

                log = true;
                /*userid = Convert.ToInt32(Reader["uid"]);
                username_ob = uname;*/
                break;
            }
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }
    public Boolean checkLoginadmin(string uname, string pass)
    {
        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select password from db_franinfo where username ='" + uname + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (pass == Convert.ToString(Reader["password"]))
            {

                log = true;

            }
        }
        Reader.Close();
        // get_user_id(uname);
        connection.Close();

        return log;
    }

    public Int16 get_min_userid()
    {
        Int16 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "SELECT  min(uid) from db_userinfo;";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt16(Reader1["min(uid)"]);
            break;
        }
        connection.Close();///
        return count;
    }

    public string get_uname(Int32 uid)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select fullname from db_userinfo where uid ='" + uid + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["fullname"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }

    public string get_hand(Int32 userid_func, string db)
    {
        string count = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "select hand from " + db + " where uid ='" + userid_func + "';";
        Reader1 = command.ExecuteReader();
      //  int i = 0;
        while (Reader1.Read())
        {
            count = Convert.ToString(Reader1["hand"]);
            break;
        }
        connection.Close();
        return count;
    }
    public Int32 get_sponid(Int32 userid_func)
    {
        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "select spon_id from db_sponsors where uid ='" + userid_func + "'";
        Reader1 = command.ExecuteReader();
      //  int i = 0;
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["spon_id"]);
            break;
        }
        connection.Close();
        return count;
    }
    public string[] get_Userinfo(Int32 user_func)
    {
        string[] count = new string[8];
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "select fullname,address,email,contact,DATE_FORMAT(DOJ ,'%e-%M-%Y %H:%i' ),DATE_FORMAT(max(db_login.last_login) ,'%e-%M-%Y %H:%i' ),db_epins.scheme,spon_id from db_sponsors,db_userinfo,db_login,db_epins where db_userinfo.uid ='" +
            user_func + "' and db_epins.uid ='" + user_func + "' and db_login.uid ='" +
            user_func + "' and db_sponsors.uid ='" + user_func + "' group by db_login.uid;";
        Reader1 = command.ExecuteReader();
        int i = 0;
        while (Reader1.Read())
        {
            count[i] = Convert.ToString(Reader1["fullname"]);
            i++;
            count[i] = Convert.ToString(Reader1["address"]);
            i++;
            count[i] = Convert.ToString(Reader1["email"]);
            i++;
            count[i] = Convert.ToString(Reader1["contact"]);
            i++;
            count[i] = Convert.ToString(Reader1["scheme"]);
            i++;
            count[i] = Convert.ToString(Reader1["DATE_FORMAT(DOJ ,'%e-%M-%Y %H:%i' )"]);
            i++;
            count[i] = Convert.ToString(Reader1["spon_id"]);
            i++;
            count[i] = Convert.ToString(Reader1["DATE_FORMAT(max(db_login.last_login) ,'%e-%M-%Y %H:%i' )"]);

            break;
        }
        connection.Close();///
        return count;
    }
    public Int16 get_userid_uname(string uname)
    {

        Int16 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select uid from db_userinfo where username ='" + uname + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt16(Reader["uid"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;


    }
    public void insert_login(Int32 userid, string ip)
    {

        string cmd = "insert into db_login (uid,ip_address)" +
                  "values('" + userid + "','" + ip + "')";

        CreateMySqlCommand(cmd, connection);

    }
    public string get_scheme(Int32 id)
    {

        string log = "";
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select scheme from db_epins where uid ='" + id + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToString(Reader["scheme"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public void CreateMySqlCommand(string myExecuteQuery, MySqlConnection myConnection)
    {
        MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, myConnection);
        myCommand.Connection.Open();
        //myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
}