﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for db_admin
/// </summary>
public class db_admin
{
   /* static string MyConString = "SERVER=209.172.55.5;" +
              "DATABASE=mlmbd;" +
              "UID=Gunner;" +
              "PASSWORD=123qweasd;Pooling=true;";*/

    static string MyConString = new database_conn().send_constr();
    MySqlConnection connection = new MySqlConnection(MyConString);
    public string replace_inject(string data)
    {
        data = data.Replace("'", "&#39");
        data = data.Replace("\"", "&#34");
        return data;
    }
    public void send_noti(string sub, string type, string msg)
    {
        msg = this.replace_inject(msg);
        sub = this.replace_inject(sub);
        string cmd = "insert into tb_notice (subject,Notice,type)" +
                    "values('" + sub + "','" + msg + "','" + type + "')";

        CreateMySqlCommand(cmd, connection);

    }
    public Int32 get_total_user()
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        //DBNull gg = null;
        command.CommandText = "SELECT count(uid) FROM db_userinfo;";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if ((Reader["count(uid)"]) != DBNull.Value)
                log = Convert.ToInt32(Reader["count(uid)"]);
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public void insert_payment(int uid, double notes,string type)
    {

        string cmd = "insert into db_payment (uid,notes,type)" +
                  "values('" + uid + "','" + notes + "','"+type+"')";

        CreateMySqlCommand(cmd, connection);

    }
    public void delete_user(Int32 id)
    {
        string cmd = "delete from db_epins where uid='" + id + "';delete from db_payment where uid='" + id + "';";

        CreateMySqlCommand(cmd, connection);

        cmd = "delete from db_userinfo where uid='" + id + "';delete from db_sponsors where uid='" + id + "';delete from db_refrs where uid='" + id + "';";
        CreateMySqlCommand(cmd, connection);

    }
    public void update_user_email(Int32 id, string email)
    {
        string cmd = "UPDATE db_userinfo SET  email =  '" + email + "' WHERE  uid ='" + id + "';";
        CreateMySqlCommand(cmd, connection);

    }
    public Int32 get_down_no(Int32 sponid)
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(uid) FROM db_sponsors where spon_id = '" + sponid + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["count(uid)"]);

        }

        connection.Close();///
        return count;
    }
    public Int32 get_ldown_no(Int32 sponid)
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(uid) FROM db_sponsors where hand = 'L' and spon_id = '" + sponid + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["count(uid)"]);

        }

        connection.Close();///
        return count;
    }
    public Int32 get_rdown_no(Int32 sponid)
    {

        Int32 count = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(uid) FROM db_sponsors where hand = 'R' and spon_id = '" + sponid + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToInt32(Reader1["count(uid)"]);

        }

        connection.Close();///
        return count;
    }
    public Boolean checkepin(Int32 epin, Int32 ecard, Int32 uid)
    {
        Boolean log = false;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select eid from db_epins where epin_no ='" + epin + "' and ecard_no='" + ecard + "' and uid = '" + uid + "';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {

            log = true;
            break;

        }
        Reader.Close();
        connection.Close();

        return log;


    }

    public Int32 get_ecard_no()
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT count(eid) FROM db_epins where month(generation_time)=month(now()) order by eid";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if ((Reader["count(eid)"]) != DBNull.Value)
                log = Convert.ToInt32(Reader["count(eid)"]);
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Int32 get_ecard_no(string user)
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT count(eid) FROM db_epins where month(generation_time)=month(now()) and created_by = 'admin' order by eid";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if ((Reader["count(eid)"]) != DBNull.Value)
                log = Convert.ToInt32(Reader["count(eid)"]);
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public Int32 get_ecard_no_used()
    {

        Int32 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "SELECT count(eid) FROM db_epins where month(generation_time)=month(now()) and uid !=0 and created_by = 'admin' order by eid";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt32(Reader["count(eid)"]);
            break;

        }
        Reader.Close();
        connection.Close();
        return log;
    }
    public List<string> get_ecard_scheme_all()
    {

        List<string> count = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT scheme FROM db_epins where month(generation_time)=month(now()) and uid !=0;";
        Reader1 = command.ExecuteReader();
        // int i = 0;
        while (Reader1.Read())
        {
            count.Add(Convert.ToString(Reader1["scheme"]));
            //i++;
        }
        connection.Close();///
        return count;
    }
    public List<Int32> get_userlist()
    {

        List<Int32> count = new List<Int32>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT uid FROM db_userinfo";
        Reader1 = command.ExecuteReader();
        // int i = 0;
        while (Reader1.Read())
        {
            count.Add(Convert.ToInt32(Reader1["uid"]));
            //i++;
        }
        connection.Close();///
        return count;
    }
    public List<string> get_ecard_scheme_used_all()
    {

        List<string> list = new List<string>();
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT scheme FROM db_epins where month(generation_time)=month(now());";
        Reader1 = command.ExecuteReader();
        //int i = 0;
        while (Reader1.Read())
        {
            list.Add(Convert.ToString(Reader1["scheme"]));
          //  i++;
        }
        connection.Close();///
        return list;
    }
    public void insert_epins_byadmin(int epin, int ecard, string scheme, int uid)
    {
        string cmd = "insert into db_epins (epin_no,ecard_no,scheme,uid,created_by)" +
              "values('" + epin + "','" + ecard + "','" + scheme + "','" + uid + "','admin')";

        CreateMySqlCommand(cmd, connection);

    }
    public string get_fullname(Int32 id)
    {
        string count = null;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();
        command.CommandText = "select fullname from db_userinfo where uid ='" + id + "';";
        Reader1 = command.ExecuteReader();
        while (Reader1.Read())
        {
            count = Convert.ToString(Reader1["fullname"]);
        }
        connection.Close();///
        return count;
    }
    public void update_variable(Int32 vid, double value)
    {
        string cmd = "UPDATE db_variables SET  value =  '" + value + "' WHERE  vid ='" + vid + "';";
        CreateMySqlCommand(cmd, connection);

    }
    public void CreateMySqlCommand(string myExecuteQuery, MySqlConnection myConnection)
    {
        MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, myConnection);
        myCommand.Connection.Open();
        //myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
}