﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MySql.Data.MySqlClient;

using System.Collections.Generic;
public class db_par
{
    static string MyConString = new database_conn().send_constr();
    MySqlConnection connection = new MySqlConnection(MyConString);



    List<Int32> Rchild_lst = new List<Int32>();
    public Int16 get_lcount(Int32 uid)
    {

        Int16 log = 0;
        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader;
        command.CommandText = "select count(pid) from db_parent where uid ='" + uid + "' and hand = 'L';";
        connection.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            log = Convert.ToInt16(Reader["count(pid)"]);
            break;

        }
        Reader.Close();
        connection.Close();

        return log;
    }
    public Int32 get_lchild_list(Int32 userid)
    {
        Int32 Lchild_lst = 0;

        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(child_id) FROM db_parent where uid = '" + userid + "' and hand = 'L';";
        Reader1 = command.ExecuteReader();

        while (Reader1.Read())
        {
            Lchild_lst= (Convert.ToInt32(Reader1["count(child_id)"]));

        }

        connection.Close();///
        return Lchild_lst;
    }
    public Int32 get_rchild_list(Int32 userid)
    {
        Int32 Lchild_lst = 0;

        MySqlCommand command = connection.CreateCommand();
        MySqlDataReader Reader1;
        connection.Open();///
        command.CommandText = "SELECT count(child_id) FROM db_parent where uid = '" + userid + "' and hand = 'R';";
        Reader1 = command.ExecuteReader();

        while (Reader1.Read())
        {
            Lchild_lst= (Convert.ToInt32(Reader1["count(child_id)"]));

        }

        connection.Close();///
        return Lchild_lst;
    }
}
